@callcenterTest  
Feature: CallCenter to place order for the customer

  
 Background: user access to ADMIN site
 Given user opens the ADMIN site
 
 
 Scenario: TC 001 Verify user is succesfully able to navigate to Admin > Call Center module
 Given Verify if user is logged in
 When user clicks CallCenter
 Then Verify call center module is open succesfully
 
 @CallCenterDebug
 Scenario: TC 002 Verify clicking Clear all button clears the form on Customer tab
 Given Verify if user is logged in
 When user clicks CallCenter
 And user enters FirstName
 |Prateek|
 And User enters LastName
 |Nehra|
 And user enters PhoneNumber
 |9687946385|
 And user enters email
 |test@test.com|
 When User Clicks Clear all button
 Then Verify the customer form gets cleared
 
@DevCallCenter  
 Scenario: TC 003 Verify clicking Clear all button clears the form on Shipping tab
 Given Verify if user is logged in
 When user clicks CallCenter
 And user enters FirstName
 |Prateek|
 And User enters LastName
 |Nehra|
 And user enters PhoneNumber
 |9687946385|
 And user enters email
 |test@test.com|
 And user clicks on Continue Button
 Then Verify Shipping tab is open
 When user Enters the address and click Find Stores
 | 87 Mill Street, Starkville, MS 39759, USA |
 When User Clicks Clear all button on shipping tab
 Then Verify the Shipping form gets cleared
 

 Scenario: TC 004 Verify clicking Cancel Order on Order tab cancels the order
 Given Verify if user is logged in
 When user clicks CallCenter
  And user enters FirstName
 |Prateek|
 And User enters LastName
 |Nehra|
 And user enters PhoneNumber
 |9687946385|
 And user enters email
 |test@test.com|
 And user clicks on Continue Button
 Then Verify Shipping tab is open
 When user Enters the address and click Find Stores
 | 87 Mill Street, Starkville, MS 39759, USA |
 And user click Delivery Button
 And user select Off Campus
 And click Continue Delivery button
 Then verify Order Tab is open
 And Click Cancel Order button
 Then verify the order gets cancelled
 
 @DevCallCenter 
 Scenario: TC 005 Enter an existing cutomer name and verify customer details gets auto populated under Select Customer drop down list box
 Given Verify if user is logged in
 When user clicks CallCenter
 And user enters FirstName
 |Prateek|
 And user waits for the list to populate
 And User enters LastName
 |Nehra|
 Then verify the existing customer information gets displayed in Select Customer drop down list
 

 Scenario: TC 006 Verify the admin is able to place a Pickup order for a new customer
 Given Verify if user is logged in
 When user clicks CallCenter
 And user enters FirstName
 |Prateek|
 And User enters LastName
 |Nehra|
 And user enters PhoneNumber
 |9687946385|
 And user enters email
 |test@test.com|
 And user clicks on Continue Button
 Then Verify Shipping tab is open
 When user Enters the address and click Find Stores
 | 87 Mill Street, Starkville, MS 39759, USA |
 And user click PickUp Button
 Then verify Order Tab is open
 When user Adds an Item
 And user selects delivery date as tomorrow
 And user select the delivery time
 And User Clicks the checkout Button
 Then Verify Billing tab is open
 And user selects Cash on Delivery as payment method
 And Click Complete order
 Then Verify Congrats message for successful Order Process
 
 @DevCallCenter 
 Scenario: TC 007 Verify the admin is able to Edit a Pickup Order from Billing Tab and removes an item from the order
 Given Verify if user is logged in
 When user clicks CallCenter
 And user enters FirstName
 |Prateek|
 And User enters LastName
 |Nehra|
 And user enters PhoneNumber
 |9687946385|
 And user enters email
 |test@test.com|
 And user clicks on Continue Button
 Then Verify Shipping tab is open
 When user Enters the address and click Find Stores
 | 87 Mill Street, Starkville, MS 39759, USA |
 And user click PickUp Button
 Then verify Order Tab is open
 When user Adds an Item
 And user Adds another Item
 And user selects delivery date as tomorrow
 And user select the delivery time
 And User Clicks the checkout Button
 Then Verify Billing tab is open
 And user clicks on Edit Order button
 And user removes an item from cart
 And User Clicks the checkout Button
 Then Verify Billing tab is open
 And user selects Cash on Delivery as payment method
 And Click Complete order
 Then Verify Congrats message for successful Order Process
 
@DevCallCenter 
 Scenario: TC 008 Verify the admin is able to Edit a Pickup Order from billing Tab and adds an item to the order
 Given Verify if user is logged in
 When user clicks CallCenter
 And user enters FirstName
 |Prateek|
 And User enters LastName
 |Nehra|
 And user enters PhoneNumber
 |9687946385|
 And user enters email
 |test@test.com|
 And user clicks on Continue Button
 Then Verify Shipping tab is open
 When user Enters the address and click Find Stores
 | 87 Mill Street, Starkville, MS 39759, USA |
 And user click PickUp Button
 Then verify Order Tab is open
 When user Adds an Item
 And user selects delivery date as tomorrow
 And user select the delivery time
 And User Clicks the checkout Button
 Then Verify Billing tab is open
 And user clicks on Edit Order button
 And user Adds another Item
 And User Clicks the checkout Button
 Then Verify Billing tab is open
 And user selects Cash on Delivery as payment method
 And Click Complete order
 Then Verify Congrats message for successful Order Process
 
 
 Scenario: TC 009 Verify the admin is able to Edit a Pickup Order from billing Tab and change the Order date and Time
 Given Verify if user is logged in
 When user clicks CallCenter
 And user enters FirstName
 |Prateek|
 And User enters LastName
 |Nehra|
 And user enters PhoneNumber
 |9687946385|
 And user enters email
 |test@test.com|
 And user clicks on Continue Button
 Then Verify Shipping tab is open
 When user Enters the address and click Find Stores
 | 87 Mill Street, Starkville, MS 39759, USA |
 And user click PickUp Button
 Then verify Order Tab is open
 When user Adds an Item
 And user selects delivery date as tomorrow
 And user select the delivery time
 And User Clicks the checkout Button
 Then Verify Billing tab is open
 And user clicks on Edit Order button
 And user edits delivery date as today
 And user select the delivery time
 And User Clicks the checkout Button
 Then Verify Billing tab is open
 And user selects Cash on Delivery as payment method
 And Click Complete order
 Then Verify Congrats message for successful Order Process
 
 
 Scenario: TC 010 Verify the admin is able to place a Delivery order for a new customer
 Given Verify if user is logged in
 When user clicks CallCenter
 And user enters random FirstName
 |Test|
 And User enters LastName
 |Name|
 And user enters PhoneNumber
 |6745989457|
 And user enters email
 |test@test123.com|
 And user clicks on Continue Button
 Then Verify Shipping tab is open
  When user Enters the address and click Find Stores
 | 87 Mill Street, Starkville, MS 39759, USA |
 And user click Delivery Button
 And user select Off Campus
 And click Continue Delivery button
 Then verify Order Tab is open
 When user Adds an Item
 And user selects delivery date as tomorrow
 And user select the delivery time
 And User Clicks the checkout Button
 Then Verify Billing tab is open
 And user selects Cash on Delivery as payment method
 And Click Complete order
 Then Verify Congrats message for successful Order Process
 

 Scenario: TC 011 Verify the admin is able to place a Delivery order for an Off Campus Location
 Given Verify if user is logged in
 When user clicks CallCenter
 And user enters FirstName
 |Prateek|
 And User enters LastName
 |Nehra|
 And user enters PhoneNumber
 |9687946385|
 And user enters email
 |test@test.com|
 And user clicks on Continue Button
 Then Verify Shipping tab is open
  When user Enters the address and click Find Stores
 | 87 Mill Street, Starkville, MS 39759, USA |
 And user click Delivery Button
 And user select Off Campus
 And click Continue Delivery button
 Then verify Order Tab is open
 When user Adds an Item
 And user selects delivery date as tomorrow
 And user select the delivery time
 And User Clicks the checkout Button
 Then Verify Billing tab is open
 And user selects Cash on Delivery as payment method
 And Click Complete order
 Then Verify Congrats message for successful Order Process
 

 Scenario: TC 012 Verify the admin is able to place a Delivery order for an On Campus Location
 Given Verify if user is logged in
 When user clicks CallCenter
 And user enters FirstName
 |Prateek|
 And User enters LastName
 |Nehra|
 And user enters PhoneNumber
 |9687946385|
 And user enters email
 |test@test.com|
 And user clicks on Continue Button
 Then Verify Shipping tab is open
 When user Enters the address and click Find Stores
 | 87 Mill Street, Starkville, MS 39759, USA |
 And user click Delivery Button
 And user select On Campus
 And user select campus building
 And click Continue Delivery button
 Then verify Order Tab is open
 When user Adds an Item
 And user selects delivery date as tomorrow
 And user select the delivery time
 And User Clicks the checkout Button
 Then Verify Billing tab is open
 And user selects Cash on Delivery as payment method
 And Click Complete order
 Then Verify Congrats message for successful Order Process
 

 Scenario: TC 013 Verify the admin is able to Edit a Delivery Order from Billing Tab and removes an item from the order and then place the order
 Given Verify if user is logged in
 When user clicks CallCenter
 And user enters FirstName
 |Prateek|
 And User enters LastName
 |Nehra|
 And user enters PhoneNumber
 |9687946385|
 And user enters email
 |test@test.com|
 And user clicks on Continue Button
 Then Verify Shipping tab is open
 When user Enters the address and click Find Stores
 | 87 Mill Street, Starkville, MS 39759, USA |
 And user click Delivery Button
 And user select Off Campus
 And click Continue Delivery button
 Then verify Order Tab is open
 When user Adds an Item
 And user Adds another Item
 And user selects delivery date as tomorrow
 And user select the delivery time
 And User Clicks the checkout Button
 Then Verify Billing tab is open
 And user clicks on Edit Order button
 And user removes an item from cart
 And User Clicks the checkout Button
 Then Verify Billing tab is open
 And user selects Cash on Delivery as payment method
 And Click Complete order
 Then Verify Congrats message for successful Order Process
 
 
 Scenario: TC 014 Verify the admin is able to Edit a Delivery Order from billing Tab and adds  an item to the order
 Given Verify if user is logged in
 When user clicks CallCenter
 And user enters FirstName
 |Prateek|
 And User enters LastName
 |Nehra|
 And user enters PhoneNumber
 |9687946385|
 And user enters email
 |test@test.com|
 And user clicks on Continue Button
 Then Verify Shipping tab is open
 When user Enters the address and click Find Stores
 | 87 Mill Street, Starkville, MS 39759, USA |
 And user click Delivery Button
 And user select Off Campus
 And click Continue Delivery button
 Then verify Order Tab is open
 When user Adds an Item
 And user selects delivery date as tomorrow
 And user select the delivery time
 And User Clicks the checkout Button
 Then Verify Billing tab is open
 And user clicks on Edit Order button
 And user Adds another Item
 And User Clicks the checkout Button
 Then Verify Billing tab is open
 And user selects Cash on Delivery as payment method
 And Click Complete order
 Then Verify Congrats message for successful Order Process
 
@DevCallCenter 
 Scenario: TC 015 Verify the admin is able to Edit a Delivery Order from billing Tab and change the Order Date and Time
 Given Verify if user is logged in
 When user clicks CallCenter
 And user enters FirstName
 |Prateek|
 And User enters LastName
 |Nehra|
 And user enters PhoneNumber
 |9687946385|
 And user enters email
 |test@test.com|
 And user clicks on Continue Button
 Then Verify Shipping tab is open
 When user Enters the address and click Find Stores
 | 87 Mill Street, Starkville, MS 39759, USA |
 And user click Delivery Button
 And user select Off Campus
 And click Continue Delivery button
 Then verify Order Tab is open
 When user Adds an Item
 And user selects delivery date as tomorrow
 And user select the delivery time
 And User Clicks the checkout Button
 Then Verify Billing tab is open
 And user clicks on Edit Order button
 And user edits delivery date as today
 And user select the delivery time
 And User Clicks the checkout Button
 Then Verify Billing tab is open
 And user selects Cash on Delivery as payment method
 And Click Complete order
 Then Verify Congrats message for successful Order Process
 

 Scenario: TC 016 Verify admin is able to place an order using Credit Card as Payment method
 Given Verify if user is logged in
 When user clicks CallCenter
 And user enters FirstName
 |Prateek|
 And User enters LastName
 |Nehra|
 And user enters PhoneNumber
 |9687946385|
 And user enters email
 |test@test.com|
 And user clicks on Continue Button
 Then Verify Shipping tab is open
  When user Enters the address and click Find Stores
 | 87 Mill Street, Starkville, MS 39759, USA |
 And user click Delivery Button
 And user select Off Campus
 And click Continue Delivery button
 Then verify Order Tab is open
 When user Adds an Item
 And user selects delivery date as tomorrow
 And user select the delivery time
 And User Clicks the checkout Button
 Then Verify Billing tab is open
 And user selects Credit Card as payment method
 And user enters CC details
      | cc no     | 4111111111111111 |
      |	cvv				| 111							 |
 And Click Complete order for CC payment
 Then Verify Congrats message for successful Order Process
 
@DevCallCenter 
 Scenario: TC 017 Verify admin is able to place an order using Cash on Dleivery as Payment method
 Given Verify if user is logged in
 When user clicks CallCenter
 And user enters FirstName
 |Prateek|
 And User enters LastName
 |Nehra|
 And user enters PhoneNumber
 |9687946385|
 And user enters email
 |test@test.com|
 And user clicks on Continue Button
 Then Verify Shipping tab is open
  When user Enters the address and click Find Stores
 | 87 Mill Street, Starkville, MS 39759, USA |
 And user click Delivery Button
 And user select Off Campus
 And click Continue Delivery button
 Then verify Order Tab is open
 When user Adds an Item
 And user selects delivery date as tomorrow
 And user select the delivery time
 And User Clicks the checkout Button
 Then Verify Billing tab is open
 And user selects Cash on Delivery as payment method
 And Click Complete order
 Then Verify Congrats message for successful Order Process
 
@DevCallCenter 
 Scenario: TC 018 Verify admin is able to place an order using Gift Card as Payment method
 Given Verify if user is logged in
 When user clicks CallCenter
 And user enters FirstName
 |Prateek|
 And User enters LastName
 |Nehra|
 And user enters PhoneNumber
 |9687946385|
 And user enters email
 |test@test.com|
 And user clicks on Continue Button
 Then Verify Shipping tab is open
  When user Enters the address and click Find Stores
 | 87 Mill Street, Starkville, MS 39759, USA |
 And user click Delivery Button
 And user select Off Campus
 And click Continue Delivery button
 Then verify Order Tab is open
 When user Adds an Item
 And user selects delivery date as tomorrow
 And user select the delivery time
 And User Clicks the checkout Button
 Then Verify Billing tab is open
 And user selects Gift Card as payment method
 And user Enters Gift card details
 | Gift Card No | 9999998888877652 |
 And Click Complete order for gift card
 Then Verify Congrats message for successful Order Process
 
@DevCallCenter  
 Scenario: TC 019 Verify admin is able to place a Pickup order for an existing customer
 Given Verify if user is logged in
 When user clicks CallCenter
 And user enters FirstName
 |Prateek|
 And user waits for the list to populate
 And User enters LastName
 |Nehra|
 And User Selects the existing customer from the customer list
 Then Verify Shipping tab is open
 And user select saved Address
 And user click PickUp Button
 Then verify Order Tab is open
 When user Adds an Item
 And user selects delivery date as tomorrow
 And user select the delivery time
 And User Clicks the checkout Button
 Then Verify Billing tab is open
 And user selects Cash on Delivery as payment method
 And Click Complete order
 Then Verify Congrats message for successful Order Process
 
 
 Scenario: TC 020 Verify admin is able to place a Delivery order for an existing customer
 Given Verify if user is logged in
 When user clicks CallCenter
 And user enters FirstName
 |Prateek|
 And user waits for the list to populate
 And User enters LastName
 |Nehra|
 And User Selects the existing customer from the customer list
 Then Verify Shipping tab is open
 And user select saved Address
 And user click Delivery Button
 And user select Off Campus
 And click Continue Delivery button
 Then verify Order Tab is open
 When user Adds an Item
 And user selects delivery date as tomorrow
 And user select the delivery time
 And User Clicks the checkout Button
 Then Verify Billing tab is open
 And user selects Cash on Delivery as payment method
 And Click Complete order
 Then Verify Congrats message for successful Order Process
 

 Scenario: TC 021 Verify after placing an order on clicking Back To Call center button, user navigates to the  Customer Information form
 Given Verify if user is logged in
 When user clicks CallCenter
 And user enters FirstName
 |Prateek|
 And User enters LastName
 |Nehra|
 And user enters PhoneNumber
 |9687946385|
 And user enters email
 |test@test.com|
 And user clicks on Continue Button
 Then Verify Shipping tab is open
  When user Enters the address and click Find Stores
 | 87 Mill Street, Starkville, MS 39759, USA |
 And user click Delivery Button
 And user select Off Campus
 And click Continue Delivery button
 Then verify Order Tab is open
 When user Adds an Item
 And user selects delivery date as tomorrow
 And user select the delivery time
 And User Clicks the checkout Button
 Then Verify Billing tab is open
 And user selects Cash on Delivery as payment method
 And Click Complete order
 Then Verify Congrats message for successful Order Process
 When User Clicks Back To Call Center button
 Then Verify call center module is open succesfully
 

 Scenario: TC 022 Verify after placing an order on clicking Start New Order button, user navigates to the Customer Information form
 Given Verify if user is logged in
 When user clicks CallCenter
 And user enters FirstName
 |Prateek|
 And User enters LastName
 |Nehra|
 And user enters PhoneNumber
 |9687946385|
 And user enters email
 |test@test.com|
 And user clicks on Continue Button
 Then Verify Shipping tab is open
  When user Enters the address and click Find Stores
 | 87 Mill Street, Starkville, MS 39759, USA |
 And user click Delivery Button
 And user select Off Campus
 And click Continue Delivery button
 Then verify Order Tab is open
 When user Adds an Item
 And user selects delivery date as tomorrow
 And user select the delivery time
 And User Clicks the checkout Button
 Then Verify Billing tab is open
 And user selects Cash on Delivery as payment method
 And Click Complete order
 Then Verify Congrats message for successful Order Process
 When User Clicks Back To Call Center button
 Then Verify call center module is open succesfully
 
@DevCallCenter 
 Scenario: TC 023 Verify admin is able to place a Delivery order using School cash as Nova Cash
 Given Verify if user is logged in
 When user clicks CallCenter
 And user enters FirstName
 |Prateek|
 And User enters LastName
 |Nehra|
 And user enters PhoneNumber
 |9687946385|
 And user enters email
 |test@test.com|
 And user clicks on Continue Button
 Then Verify Shipping tab is open
 When user Enters the address and click Find Stores
 | 1084 lancaster avenue bryn mawr |
 And user click Delivery Button
 And user select On Campus
 And user select campus building
 And click Continue Delivery button
 Then verify Order Tab is open
 When user Adds an Item
 And user selects delivery date as tomorrow
 And user select the delivery time
 And User Clicks the checkout Button
 Then Verify Billing tab is open
 When user selects Nova Cash as payment method
 And user enters School Cash
 |6010000000000000|
 And Click Complete order for School Cash
 Then Verify Congrats message for successful Order Process
 
 @DevCallCenter 
 Scenario: TC 024 Verify Cookie Cake order requires 1 hr lead time
 Given Verify if user is logged in
 When user clicks CallCenter
 And user enters FirstName
 |Prateek|
 And User enters LastName
 |Nehra|
 And user enters PhoneNumber
 |9687946385|
 And user enters email
 |test@test.com|
 And user clicks on Continue Button
 Then Verify Shipping tab is open
 When user Enters the address and click Find Stores
 | 87 Mill Street, Starkville, MS 39759, USA |
 And user click Delivery Button
 And user select Off Campus
 And click Continue Delivery button
 Then verify Order Tab is open
 When user Adds Cookie Cake to cart
 And user selects delivery date as today
 And user select the delivery time as store open time
 And User Clicks the checkout Button
 Then verify validation message stating Cake orders take longer to bake. is displayed

 Scenario: TC 025 Verify that order can be placed 6 months in advance
 Given Verify if user is logged in
 When user clicks CallCenter
 And user enters FirstName
 |Prateek|
 And User enters LastName
 |Nehra|
 And user enters PhoneNumber
 |9687946385|
 And user enters email
 |test@test.com|
 And user clicks on Continue Button
 Then Verify Shipping tab is open
  When user Enters the address and click Find Stores
 | 87 Mill Street, Starkville, MS 39759, USA |
 And user click Delivery Button
 And user select Off Campus
 And click Continue Delivery button
 Then verify Order Tab is open
 When user Adds an Item
 And user selects delivery date as 6 months from today
 And user select the delivery time
 And User Clicks the checkout Button
 Then Verify Billing tab is open
 And user selects Cash on Delivery as payment method
 And Click Complete order
 Then Verify Congrats message for successful Order Process

@DevCallCenter 
 Scenario: TC 026 Verify that Cookie Cake can't be placed for pickup until 1 hr after the store opens.
 Given Verify if user is logged in
 When user clicks CallCenter
 And user enters FirstName
 |Prateek|
 And User enters LastName
 |Nehra|
 And user enters PhoneNumber
 |9687946385|
 And user enters email
 |test@test.com|
 And user clicks on Continue Button
 Then Verify Shipping tab is open
 When user Enters the address and click Find Stores
 | 87 Mill Street, Starkville, MS 39759, USA |
 And user click PickUp Button
 Then verify Order Tab is open
 When user Adds Cookie Cake to cart
 And user selects delivery date as today
 And user select the delivery time as store open time
 And User Clicks the checkout Button
 Then verify validation message stating Cake orders take longer to bake. is displayed
 