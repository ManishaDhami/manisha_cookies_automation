@Menu
Feature: Menu

  Background: 
    Given User opens the Website

@DevMenu  
  Scenario: Verify user is able to access Menu Tab
    And user clicks on Menu tab
    Then user should be able to access Menu tab successfully

@DevMenu
  Scenario: Verify menu images of all products are displayed on menu page
  And user clicks on Menu tab
	Then all product menu images should be displayed