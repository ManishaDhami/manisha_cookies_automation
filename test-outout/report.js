$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Order.feature");
formatter.feature({
  "line": 2,
  "name": "Order",
  "description": "",
  "id": "order",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@OrderGuest"
    }
  ]
});
formatter.background({
  "line": 4,
  "name": "user access to website",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "User opens the Website",
  "keyword": "Given "
});
formatter.match({
  "location": "Step_SignIn.user_opens_the_Website()"
});
formatter.result({
  "duration": 45484154300,
  "status": "passed"
});
formatter.scenario({
  "line": 648,
  "name": "TC086 - Guest User - Verify guest user is able to update pickup time from the cart for Pickup Order",
  "description": "",
  "id": "order;tc086---guest-user---verify-guest-user-is-able-to-update-pickup-time-from-the-cart-for-pickup-order",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 647,
      "name": "@OrderGuestDebug"
    }
  ]
});
formatter.step({
  "line": 649,
  "name": "user clicks on Order button",
  "keyword": "Given "
});
formatter.step({
  "line": 650,
  "name": "user enters the address",
  "rows": [
    {
      "cells": [
        "address",
        "4319 Main St Philadelphia, PA"
      ],
      "line": 651
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 652,
  "name": "user clicks on Pickup button",
  "keyword": "And "
});
formatter.step({
  "line": 653,
  "name": "user select Date and Time from Calendar",
  "keyword": "And "
});
formatter.step({
  "line": 654,
  "name": "user clicks on Continue",
  "keyword": "And "
});
formatter.step({
  "line": 655,
  "name": "On Menu Page click on The Major Rager",
  "keyword": "And "
});
formatter.step({
  "line": 656,
  "name": "user clicks on add product",
  "keyword": "And "
});
formatter.step({
  "line": 657,
  "name": "user goes to the cart",
  "keyword": "And "
});
formatter.step({
  "line": 658,
  "name": "user clicks on Pickup time \u0026 date displayed on the cart top",
  "keyword": "And "
});
formatter.step({
  "line": 659,
  "name": "user changes date",
  "keyword": "And "
});
formatter.step({
  "line": 660,
  "name": "user changes time",
  "keyword": "And "
});
formatter.step({
  "line": 661,
  "name": "user clicks Update button",
  "keyword": "And "
});
formatter.step({
  "line": 662,
  "name": "updated pickup date \u0026 time should be displayed on the cart top",
  "keyword": "Then "
});
formatter.match({
  "location": "Step_Order.user_clicks_on_Order_button()"
});
formatter.result({
  "duration": 13196412400,
  "status": "passed"
});
formatter.match({
  "location": "Step_Order.user_enters_the_address(DataTable)"
});
formatter.result({
  "duration": 14449607600,
  "status": "passed"
});
formatter.match({
  "location": "Step_Order.pickup()"
});
formatter.result({
  "duration": 6170438900,
  "status": "passed"
});
formatter.match({
  "location": "Step_Order.user_select_Date_and_Time_from_Calendar()"
});
formatter.result({
  "duration": 41312982700,
  "status": "passed"
});
formatter.match({
  "location": "Step_Order.user_clicks_on_Continue()"
});
formatter.result({
  "duration": 10311325100,
  "status": "passed"
});
formatter.match({
  "location": "Step_Order.major_rager()"
});
formatter.result({
  "duration": 5603459200,
  "status": "passed"
});
formatter.match({
  "location": "Step_Order.user_clicks_on_add_product()"
});
formatter.result({
  "duration": 597406600,
  "status": "passed"
});
formatter.match({
  "location": "Step_Order.user_goes_to_the_cart()"
});
formatter.result({
  "duration": 2299437900,
  "status": "passed"
});
formatter.match({
  "location": "Step_Order.user_clicks_on_Pickup_time_date_displayed_on_the_cart_top()"
});
formatter.result({
  "duration": 164080800,
  "status": "passed"
});
formatter.match({
  "location": "Step_Order.user_changes_date()"
});
formatter.result({
  "duration": 266661300,
  "status": "passed"
});
formatter.match({
  "location": "Step_Order.user_changes_time()"
});
formatter.result({
  "duration": 13168853700,
  "status": "passed"
});
formatter.match({
  "location": "Step_Order.user_clicks_Update_button()"
});
formatter.result({
  "duration": 1603951500,
  "status": "passed"
});
formatter.match({
  "location": "Step_Order.updated_pickup_date_time_should_be_displayed_on_the_cart_top()"
});
formatter.result({
  "duration": 5301866800,
  "status": "passed"
});
formatter.after({
  "duration": 24739600,
  "status": "passed"
});
});