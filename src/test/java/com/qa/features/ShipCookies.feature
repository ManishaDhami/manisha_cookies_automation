@ShipCookies
Feature: Ship Cookies - This is used to place shipping order

  Background: user access to website
    Given User opens the Website
    And if user is already logged in- just logout



  Scenario: TC035 - Verify whether  user is able to access Ship cookies page
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    Then ship cookies page should be displayed successfully


  Scenario: TC033 - Verify Guest User gets a POP UP when applying any coupon.
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Twelve cookies Gift box
    And user clicks on add product
    And user goes to the cart
    And user enters Coupon
      | Amount code | TestCoupon |
    And user clicks Apply button
    Then Error popup stating You must be logged in to use this coupon should appear


  Scenario: TC034 - Verify Guest user is not able to see Saved Card option on Checkout Page  # associated bug : 6020
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Twelve cookies Gift box
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    Then saved credit card should not be visibile to the user
    

  Scenario: TC036 - Verify User is able to place order with Gift card
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Twelve cookies Gift box
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under recipient Delivery Info & your Info
      | recepient name     | Theresa Ainsworth |
      | recepient phone    |        2345678981 |
      | customer full name | Prateek Nehra     |
      | customer phone     |        3457689024 |
      | customer emailID   | pnehra@judge.com  |
    And user enters shipping address
      | Delivery Address | 2227 Massachusetts Avenue, Lexington, MA |
    And user selects Payment Method as Gift card
    And user enters Gift card number
      | Gift Card | 9999998888877651 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary


  Scenario: TC037 - Verify User is able to place order with Credit card
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Twelve cookies Gift box
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under recipient Delivery Info & your Info
      | recepient name     | Theresa Ainsworth |
      | recepient phone    |        2345678981 |
      | customer full name | Prateek Nehra     |
      | customer phone     |        3457689024 |
      | customer emailID   | pnehra@judge.com  |
    And user enters shipping address
      | Delivery Address | 1084 lancaster avenue bryn mawr |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 03 20            |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary


  Scenario: TC038 - Verify user is able to add TRIPLE LAYERED COOKIE CAKE in Cart through + option and Order Successfully
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects triple layer cookie cake
    And user adds cookies by clicking +
    And user clicks Add Product button
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under recipient Delivery Info & your Info
      | recepient name     | Theresa Ainsworth |
      | recepient phone    |        2345678981 |
      | customer full name | Prateek Nehra     |
      | customer phone     |        3457689024 |
      | customer emailID   | pnehra@judge.com  |
    And user enters shipping address
      | Delivery Address | 1084 lancaster avenue bryn mawr |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 03 20            |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary

  Scenario: TC040 - Verify user is able to add 18 Cookie Gift Box in Cart through Pick For Me option and Order Successfully
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Eighteen cookies Gift box
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under recipient Delivery Info & your Info
      | recepient name     | Theresa Ainsworth |
      | recepient phone    |        2345678981 |
      | customer full name | Prateek Nehra     |
      | customer phone     |        3457689024 |
      | customer emailID   | pnehra@judge.com  |
    And user enters shipping address
      | Delivery Address | 2227 Massachusetts Avenue, Lexington, MA |
    And user selects Payment Method as Gift card
    And user enters Gift card number
      | Gift Card | 9999998888877651 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary

  Scenario: TC041 - Verify user is able to add 24 Cookie Gift Box in Cart through Pick For Me option and Order Successfully
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Twenty Four cookies Gift box
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under recipient Delivery Info & your Info
      | recepient name     | Theresa Ainsworth |
      | recepient phone    |        2345678981 |
      | customer full name | Prateek Nehra     |
      | customer phone     |        3457689024 |
      | customer emailID   | pnehra@judge.com  |
    And user enters shipping address
      | Delivery Address | 2227 Massachusetts Avenue, Lexington, MA |
    And user selects Payment Method as Gift card
    And user enters Gift card number
      | Gift Card | 9999998888877651 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
	

  Scenario: TC043 - Verify user is able to add 12 Cookie Gift Box in Cart by selecting manually and order successfully
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Twelve cookies Gift box
    And user selects cookies manually
    And user clicks Add Product button
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under recipient Delivery Info & your Info
      | recepient name     | Theresa Ainsworth |
      | recepient phone    |        2345678981 |
      | customer full name | Prateek Nehra     |
      | customer phone     |        3457689024 |
      | customer emailID   | pnehra@judge.com  |
    And user enters shipping address
      | Delivery Address | 1084 lancaster avenue bryn mawr |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 03 20            |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary


  Scenario: TC044 - Verify user is able to add 18 Cookie Gift Box in Cart by selecting manually and order successfully
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Eighteen cookies Gift box
    And user selects cookies manually
    And user clicks Add Product button
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under recipient Delivery Info & your Info
      | recepient name     | Theresa Ainsworth |
      | recepient phone    |        2345678981 |
      | customer full name | Prateek Nehra     |
      | customer phone     |        3457689024 |
      | customer emailID   | pnehra@judge.com  |
    And user enters shipping address
      | Delivery Address | 2227 Massachusetts Avenue, Lexington, MA |
    And user selects Payment Method as Gift card
    And user enters Gift card number
      | Gift Card | 9999998888877651 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary


  Scenario: TC045 - Verify user is able to add 24 Cookie Gift Box in Cart by selecting manually and order successfully
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Twenty Four cookies Gift box
    And user selects cookies manually
    And user clicks Add Product button
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under recipient Delivery Info & your Info
      | recepient name     | Theresa Ainsworth |
      | recepient phone    |        2345678981 |
      | customer full name | Prateek Nehra     |
      | customer phone     |        3457689024 |
      | customer emailID   | pnehra@judge.com  |
    And user enters shipping address
      | Delivery Address | 2227 Massachusetts Avenue, Lexington, MA |
    And user selects Payment Method as Gift card
    And user enters Gift card number
      | Gift Card | 9999998888877651 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary


  Scenario: TC046 - verify user is able to deselect Triple Layer Cookie Cake manually
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects triple layer cookie cake
    And user selects cookies manually
    And user deselects cookies manually
    Then Make required choices button should appear indicating zero selections

  Scenario: TC047 - verify user is able to deselect 12 Cookies Gift box  cookies manually
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Twelve cookies Gift box
    And user selects cookies manually
    And user deselects cookies manually
    Then Make required choices button should appear indicating zero selections

  Scenario: TC048 - verify user is able to deselect 18 Cookies Gift box manually
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Eighteen cookies Gift box
    And user selects cookies manually
    And user deselects cookies manually
    Then Make required choices button should appear indicating zero selections

  Scenario: TC049 - verify user is able to deselect 24 Cookies Gift box manually
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Twenty Four cookies Gift box
    And user selects cookies manually
    And user deselects cookies manually
    Then Make required choices button should appear indicating zero selections


  Scenario: TC050 - verify user is able to close Triple Layer Cookie Cake  menu
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects triple layer cookie cake
    And user clicks on  close button
    Then product menu should close

  Scenario: TC051 - verify user is able to close 12 Cookie Gift Box menu
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Twelve cookies Gift box
    And user clicks on  close button
    Then product menu should close

  Scenario: TC052 - verify user is able to close 18 Cookie Gift Box menu
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Eighteen cookies Gift box
    And user clicks on  close button
    Then product menu should close

    Scenario: TC053 - verify user is able to close 24  Cookie Gift Box Menu
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Twenty Four cookies Gift box
    And user clicks on  close button
    Then product menu should close
    

    Scenario: TC054 - Verify menu is displayed for Triple Layer Cookie Cake   
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects triple layer cookie cake
    Then menu options should be displayed
      | Chocolate Chunk        		|
      | Classic with M&M's     		|
      | Double Chocolate Chunk 		|
      | Double Chocolate Mint  		|
      | Oatmeal Raisin         		|
      | Peanut Butter Chip     		|
      | Red Velvet								|
      | Snickerdoodle          		|
      | Sugar                  		|
      | White Chocolate Macadamia |

    Scenario: TC055 - Verify menu is displayed for 12 Cookie Gift Box
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Twelve cookies Gift box
    Then menu options should be displayed
      | Chocolate Chunk        		|
      | Classic with M&M's     		|
      | Double Chocolate Chunk 		|
      | Double Chocolate Mint  		|
      | Oatmeal Raisin         		|
      | Peanut Butter Chip     		|
      | Red Velvet								|
      | Snickerdoodle          		|
      | Sugar                  		|
      | White Chocolate Macadamia |

    Scenario: TC056 - Verify menu is displayed for 18 Cookie Gift Box
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Eighteen cookies Gift box
    Then menu options should be displayed
      | Chocolate Chunk        		|
      | Classic with M&M's     		|
      | Double Chocolate Chunk 		|
      | Double Chocolate Mint  		|
      | Oatmeal Raisin         		|
      | Peanut Butter Chip     		|
      | Red Velvet								|
      | Snickerdoodle          		|
      | Sugar                  		|
      | White Chocolate Macadamia |

    Scenario: TC057 - Verify menu is displayed for 24 Cookie Gift Box
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Twenty Four cookies Gift box
    Then menu options should be displayed
      | Chocolate Chunk        		|
      | Classic with M&M's     		|
      | Double Chocolate Chunk 		|
      | Double Chocolate Mint  		|
      | Oatmeal Raisin         		|
      | Peanut Butter Chip     		|
      | Red Velvet								|
      | Snickerdoodle          		|
      | Sugar                  		|
      | White Chocolate Macadamia |


    Scenario: TC058 - Verify user is able to add VEGAN/GLUTEN FREE 12 COOKIE GIFT BOX in cart by clicking on '+' and order successfully
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user clicks + of Vegan/ Gluten Free Twelve Cookie Gift Box
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under recipient Delivery Info & your Info
      | recepient name     | Theresa Ainsworth |
      | recepient phone    |        2345678981 |
      | customer full name | Prateek Nehra     |
      | customer phone     |        3457689024 |
      | customer emailID   | pnehra@judge.com  |
    And user enters shipping address
      | Delivery Address | 2227 Massachusetts Avenue, Lexington, MA |
    And user selects Payment Method as Gift card
    And user enters Gift card number
      | Gift Card | 9999998888877651 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary

    Scenario: TC059 - Verify user is able to add VEGAN/GLUTEN FREE 24 COOKIE GIFT BOX in cart by clicking on '+' and order successfully
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user clicks + of Vegan/ Gluten Free Twenty Four Cookie Gift Box
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under recipient Delivery Info & your Info
      | recepient name     | Theresa Ainsworth |
      | recepient phone    |        2345678981 |
      | customer full name | Prateek Nehra     |
      | customer phone     |        3457689024 |
      | customer emailID   | pnehra@judge.com  |
    And user enters shipping address
      | Delivery Address | 2227 Massachusetts Avenue, Lexington, MA |
    And user selects Payment Method as Gift card
    And user enters Gift card number
      | Gift Card | 9999998888877651 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary

@ShipCookies111
    Scenario: TC060 - Verify user is able to select Quantity for every product from their respective menu page
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects triple layer cookie cake
    And user clicks quantity
    And user selects cookies manually
    And user clicks Add Product button
    And user goes to the cart
    Then updated quantity of the product should reflect in the cart
    And user selects Twelve cookies Gift box
    And user clicks quantity
    And user selects cookies manually
    And user clicks Add Product button
    And user goes to the cart again
    Then updated quantity of the product should reflect in the cart
    And user selects Eighteen cookies Gift box
    And user clicks quantity
    And user selects cookies manually
    And user clicks Add Product button
    And user goes to the cart again
    Then updated quantity of the product should reflect in the cart
    And user selects Twenty Four cookies Gift box
    And user clicks quantity
    And user selects cookies manually
    And user clicks Add Product button
    And user goes to the cart again
    Then updated quantity of the product should reflect in the cart
    And user selects VEGAN/GLUTEN FREE TWELVE COOKIE GIFT BOX
    And user clicks quantity
    And user clicks Add Product button
    And user goes to the cart again
    Then updated quantity of the product should reflect in the cart
    And user selects VEGAN/GLUTEN FREE TWENTY FOUR COOKIE GIFT BOX
    And user clicks quantity
    #And user selects cookies manually
    And user clicks Add Product button
    And user goes to the cart again
    Then updated quantity of the product should reflect in the cart

    Scenario: TC061 - Verify User is able to see 3 delivery options on Checkout Page
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Eighteen cookies Gift box
    And user selects cookies manually
    And user clicks Add Product button
    And user goes to the cart
    And user clicks on the checkout button
    And user enters shipping address
      | Delivery Address | 1084 lancaster avenue Bryn Mawr |
    Then shipping methods should be displayed


    Scenario: TC063 - Verify user is not able to apply Percentage coupon as guest user
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Twelve cookies Gift box
    And user clicks on add product
    And user goes to the cart
    And user enters Coupon
      | Percentage code | CYBER |
    And user clicks Apply button
    Then Error popup stating You must be logged in to use this coupon should appear

    Scenario: TC064 - Verify user is not able to apply Free Delivery coupon as guest user
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Twelve cookies Gift box
    And user clicks on add product
    And user goes to the cart
    And user enters Coupon
       | Free Delivery code | DELIVERY10 |
    And user clicks Apply button
    Then Error popup stating You must be logged in to use this coupon should appear
  
    Scenario: TC065 - Verify user is not able to apply Product coupon as guest user
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Twelve cookies Gift box
    And user clicks on add product
    And user goes to the cart
    And user enters Coupon
       | Product coupon | 6 Free Cookies |
    And user clicks Apply button
    Then Error popup stating You must be logged in to use this coupon should appear
 
     
    Scenario: TC066 - Verify guest user gets the suggestion to place Delivery order if address entered on checkout page falls in the delivery range
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Eighteen cookies Gift box
    And user selects cookies manually
    And user clicks Add Product button
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under recipient Delivery Info & your Info
      | recepient name     | Theresa Ainsworth |
      | recepient phone    |        2345678981 |
      | customer full name | Prateek Nehra     |
      | customer phone     |        3457689024 |
      | customer emailID   | pnehra@judge.com  |
    And user enters shipping address
      | Delivery Address | 1084 lancaster avenue Bryn Mawr |
    Then user should get the suggestion to place Delivery order

    Scenario: TC068 - Verify link stating - Hey, it looks like you are in delivery range! Click here to get your order delivered, directs to Orders page and cart should get cleared
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Eighteen cookies Gift box
    And user selects cookies manually
    And user clicks Add Product button
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under recipient Delivery Info & your Info
      | recepient name     | Theresa Ainsworth |
      | recepient phone    |        2345678981 |
      | customer full name | Prateek Nehra     |
      | customer phone     |        3457689024 |
      | customer emailID   | pnehra@judge.com  |
    And user enters shipping address
      | Delivery Address | 1084 lancaster avenue Bryn Mawr |
    And user clicks on here link displayed in suggession under address box
    Then popup should appear
    And on clicking Okay button
    Then cart should get cleared
 
     
    Scenario: TC071 - Verify user is able to update product of Triple layer Cookie Cake
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects triple layer cookie cake
    And user adds cookies by clicking +
    And user clicks Add Product button
    And user goes to the cart
    And user clicks on the added product in the cart
    And user selects a quantity from the dropdown
    And user clicks update product button
    Then updated quantity of the product should reflect in the cart
    
  
    Scenario: TC072 - Verify user is able to update product of 12 Cookie Gift Box 
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Twelve cookies Gift box
    And user clicks on add product
    And user goes to the cart
    And user clicks on the added product in the cart
    And user selects a quantity from the dropdown
    And user clicks update product button
    Then updated quantity of the product should reflect in the cart
    

    Scenario: TC073 - Verify user is able to update product of 18 Cookie Gift Box
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Eighteen cookies Gift box
    And user clicks on add product
    And user goes to the cart
    And user clicks on the added product in the cart
    And user selects a quantity from the dropdown
    And user clicks update product button
    Then updated quantity of the product should reflect in the cart
 
    Scenario: TC074 - Verify user is able to update product of 24 Cookie Gift Box
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Twenty Four cookies Gift box
    And user clicks on add product
    And user goes to the cart
    And user clicks on the added product in the cart
    And user selects a quantity from the dropdown
    And user clicks update product button
    Then updated quantity of the product should reflect in the cart
    

    Scenario: TC075 - Verify user is able to update product of VEGAN/GLUTEN FREE 12 COOKIE GIFT BOX 
     When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user clicks + of Vegan/ Gluten Free Twelve Cookie Gift Box
    And user goes to the cart
    And user clicks on the added product in the cart
    And user selects a quantity from the dropdown
    And user clicks update product button
    Then updated quantity of the product should reflect in the cart
 
    Scenario: TC076 - Verify user is able to update product of VEGAN/GLUTEN FREE 24 COOKIE GIFT BOX 
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user clicks + of Vegan/ Gluten Free Twenty Four Cookie Gift Box
    And user goes to the cart
    And user clicks on the added product in the cart
    And user selects a quantity from the dropdown
    And user clicks update product button
    Then updated quantity of the product should reflect in the cart
   
   
    Scenario: TC078 - Verify user is able to update product of Twelve Cookie Gift Box
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Twelve cookies Gift box
    And user clicks on add product
    And user goes to the cart
    And user clicks on the added product in the cart
    And user selects a quantity from the dropdown
    And user clicks update product button
    Then updated quantity of the product should reflect in the cart 


    