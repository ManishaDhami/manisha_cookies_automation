@Tracker

Feature: Tracker - This module is used to track Delivery & Pickup Orders.


Background: user access to website
    Given User opens the Website
    And if user is already logged in- just logout
    
    Scenario: TC001 - Guest User -Verify user is able to access Cookie Tracker Page
    And user clicks Tracker tab
    Then Tracker page should be displayed successfully to the user


    Scenario: TC002 - Guest User -Verify Placeholder text "Tracker ID" should be displayed in the search textbox
    And user clicks Tracker tab
    Then placeholder text Tracker ID should be displayed in textbox
    
    
    Scenario: TC003 - Guest User -Verify Placeholder text "Tracker ID" disappears on entering data in the textbox
     And user clicks Tracker tab
    And user enters tracking ID
    |01738366822bd7f7|
     Then placeholder text Tracker ID should disappear in textbox
       
    Scenario: TC004 - Guest User -Verify user is able to track order with valid Tracking ID
    And user clicks Tracker tab
    And user enters tracking ID
    |dd33d346b9be1533|
    And user clicks track order button on tracker page
    Then Tracking ID should be displayed along with status
        
    Scenario: TC005 - Guest User -Verify user is not able to track order with invalid Tracking ID
    And user clicks Tracker tab
    And user enters tracking ID
    |djkldg#4d9g335|
    And user clicks track order button on tracker page
    Then Text should display - no order found
       
    Scenario: TC006 - Guest User -Verify user is not able to search with blank request
     And user clicks Tracker tab
     And user clicks track order button on tracker page
     Then popup appears stating
     |Tracking ID cannot be empty!|
          
     Scenario: TC007 - Guest User -Verify user is able to see three options for order status - Baking, Out for Delivery, You're up Next on Google Map
     And user clicks Tracker tab
     And user enters tracking ID
    |01738366822bd7f7|
    And user clicks track order button on tracker page
    Then user should be able to see three options for order status - Baking, Out for Delivery, You're up Next on Google Map
    
    
    Scenario: TC008 - Guest User -Verify Placeholder text "Tracking ID" should be displayed in the search textbox
    And user clicks Tracker tab
    And user enters tracking ID
    |01738366822bd7f7|
    And user clicks track order button on tracker page
    Then Tracking ID should be displayed along with status
    
    
    Scenario: TC013 - Guest User -Verify user is able to track order on google maps
    And user clicks Tracker tab
    And user enters tracking ID
    |01738366822bd7f7|
    And user clicks track order button on tracker page
    Then store pointer should be displayed on google maps
       
    Scenario: TC014 - Guest User -Verify tracker gets updated in accordance with pickup and delivery tracking id respectively
 		And user clicks Tracker tab
    And user enters tracking ID
    |dd33d346b9be1533|
   	And user clicks track order button on tracker page
   	And user removes tracker ID for Delivery 
    And user enters tracking ID
   	|01738366822bd7f7|
    And user clicks track order button on tracker page
    Then Tracking ID should be displayed along with status
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    