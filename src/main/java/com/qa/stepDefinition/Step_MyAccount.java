package com.qa.stepDefinition;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.qa.utils.TestBase;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import com.qa.pages.SignUp;
import com.qa.pages.Order;
import com.qa.pages.SignIn;

public class Step_MyAccount extends TestBase{
	
	// public WebDriver driver;
		String url= getUrl();
		/*
		 * public step_signin() { driver = Hooks.driver; // TODO Auto-generated
		 * constructor stub }
		 */
		//ConfigFileReader configFileReader = new ConfigFileReader();
		 WebDriver driver = getDriver();
		 WebDriverWait wait = new WebDriverWait(driver, 12);
		// public Map<String, String> url;
		SignIn signIn = new SignIn(driver);
		Order order = new Order(driver);
		SignUp signup = new SignUp(driver);
		
		
		int initialOrderCount;
		int initialOrderCount1;
		
		
		
		@Then("^User should be redirected to Order History Page$")
		public void user_should_be_redirected_to_order_History_Page() throws Throwable {
			
			try{
				
				wait.until(ExpectedConditions.elementToBeClickable(signIn.orderHistory));
				Assert.assertTrue(signIn.orderHistory.isDisplayed());
				System.out.println("User is able to access Order History Page");

			}finally{
				wait.until(ExpectedConditions.elementToBeClickable(signIn.login_text));
				signIn.login_text.click();
				wait.until(ExpectedConditions.elementToBeClickable(signIn.logout));
				signIn.logout.click();
			}
		}
		
		
		@Then("^First Name,Last Name ,Email & Phone should be displayed under My Account Section.$")
		public void firstName_LastName_Email_Should_be_displayed() throws Throwable {
			
			try{
				
				wait.until(ExpectedConditions.elementToBeClickable(signIn.myAccountDetails));
				String str = new String();
				str = signIn.myAccountDetails.getText();
				System.out.println(str);
				if (str.contains("PRATEEK NEHRA")){
					System.out.println("First and Last Name is Displayed");
				}
				if (str.contains("icprateeknehra@gmail.com")){
						System.out.println("emailID is Displayed");
				}
				if (str.contains("6467879865")){
							System.out.println("Phone Number is Displayed");
				}
				else {
					System.out.println("Test Case Failed");
				}
				

			}finally{
				wait.until(ExpectedConditions.elementToBeClickable(signIn.login_text));
				signIn.login_text.click();
				wait.until(ExpectedConditions.elementToBeClickable(signIn.logout));
				signIn.logout.click();
			}
		}
		
		@Then("^Address should be displayed under Address Book Section.$")
		public void address_should_be_displayed() throws Throwable {
			
			try{
				
				wait.until(ExpectedConditions.elementToBeClickable(signIn.addressDetails));
				String str = new String();
				str = signIn.addressDetails.getText();
				System.out.println(str);
				if (str.contains("1084 East Lancaster Avenue")){
					System.out.println("Address is displayed");
				}
				
				else {
					System.out.println("Test Case Failed");
				}
				

			}finally{
				wait.until(ExpectedConditions.elementToBeClickable(signIn.login_text));
				signIn.login_text.click();
				wait.until(ExpectedConditions.elementToBeClickable(signIn.logout));
				signIn.logout.click();
			}
		}
		
		
		@Then("^Loyalty No., points,Rewards and Next reward should be displayed under Loyalty section.$")
		public void loyaltyNo_points_rewards() throws Throwable {
			
			try{
				
				wait.until(ExpectedConditions.elementToBeClickable(signIn.loyalty));
				
				List<WebElement> loyaltyList = signIn.loyaltyDetails;
				
				WebElement firstElement = loyaltyList.get(0);
				WebElement secondElement = loyaltyList.get(1);
				WebElement thirdElement = loyaltyList.get(2);
				WebElement fouthElement = loyaltyList.get(3);
				
				String loyaltyNumber = firstElement.getText();
				String points = secondElement.getText();
				String rewards = thirdElement.getText();
				String nextRewards = fouthElement.getText();
				
				if(loyaltyNumber.contains("Loyalty Number")) {
					System.out.println("Loyalty Number is displayed");
				}if(points.contains("Points")) {
					System.out.println("Points is displayed");
				}if(rewards.contains("Rewards")) {
					System.out.println("Rewards is displayed");
				}if(nextRewards.contains("Next Reward")) {
					System.out.println("Next Reward is displayed");
				}
				
				else {
					System.out.println("Test Case Failed");
				}
				

			}finally{
				wait.until(ExpectedConditions.elementToBeClickable(signIn.login_text));
				signIn.login_text.click();
				wait.until(ExpectedConditions.elementToBeClickable(signIn.logout));
				signIn.logout.click();
			}
		}
		
		
		@Then("^Order History should be displayed in descensing order number.$")
		public void order_History_should_be_displayed_decending() throws Throwable {
			
			try{
				
				wait.until(ExpectedConditions.elementToBeClickable(signIn.orderHistory1));
				String str = new String();
				str = signIn.orderHistory1.getText();
				System.out.println(str);
				String [] lines = str.split("\\n");
				String line2Order1 = lines[1];
				System.out.println(line2Order1);
				String orderNumber1 = line2Order1.substring(7,17);
				System.out.println(orderNumber1);
				
				String str2 = new String();
				str = signIn.orderHistory2.getText();
				System.out.println(str2);
				String [] lines1 = str.split("\\n");
				String line2Order2 = lines1[1];
				System.out.println(line2Order2);
				String orderNumber2 = line2Order2.substring(7,17);
				System.out.println(orderNumber2);
				
				int order1 = Integer.parseInt(orderNumber1);
				int order2 = Integer.parseInt(orderNumber2);
				
				Assert.assertTrue(order1>order2);
				System.out.println("Order History displayed in descensing order number");
				
			}finally{
				wait.until(ExpectedConditions.elementToBeClickable(signIn.login_text));
				signIn.login_text.click();
				wait.until(ExpectedConditions.elementToBeClickable(signIn.logout));
				signIn.logout.click();
			}
		}
		
		@Then("^Click Load More link under the orders$")
		public void click_load_more_link_under_orders() throws Throwable {
			
			
		}
		
		
		@Then("^Entire Order History should be displayed$")
		public void entire_Order_History_shouldBe_Displayed() throws Throwable {
			
			try{
				wait.until(ExpectedConditions.elementToBeClickable(signIn.orderLoadMore));
				List<WebElement> ordersList = signIn.numberofOrders;
				
				int initialOrderCount = ordersList.size();
				
				signIn.orderLoadMore.click();
				Thread.sleep(2000);
				
				int initialOrderCount1 = ordersList.size();
				
				System.out.println("InititalCount is "+initialOrderCount     +"updated count is"+initialOrderCount1);
				
				Assert.assertTrue(initialOrderCount1>initialOrderCount);
				System.out.println("Entire Order history is displayed.");

			}finally{
				wait.until(ExpectedConditions.elementToBeClickable(signIn.login_text));
				signIn.login_text.click();
				wait.until(ExpectedConditions.elementToBeClickable(signIn.logout));
				signIn.logout.click();
			}
		}
	    
	    @And("^Click on the edit button to update the profile$")
		public void click_On_Edit_Profile_to_Update_the_Profile() throws Throwable {						
				wait.until(ExpectedConditions.elementToBeClickable(signIn.editProfile));
				signIn.editProfile.click();
		}
	    
	    @Then("^Update FirstName, LastName, Email, Phone and click Update Profile button$")
	    	    
		public void update_FirstName_LastName_Email_Phone(DataTable table) throws Throwable {	
	    	
	    		List<List<String>> data = table.raw();
				wait.until(ExpectedConditions.elementToBeClickable(signIn.firstName));
				Thread.sleep(5000);
//				signIn.firstName.click();
				String selectAll = Keys.chord(Keys.CONTROL, "a");				
				signIn.firstName.sendKeys(selectAll,Keys.DELETE);
				wait.until(ExpectedConditions.elementToBeClickable(signIn.firstName));
				String fName = data.get(0).get(1);
				String FirstName = signIn.randomNumber(fName);				
				signIn.firstName.sendKeys(FirstName);
				

				signIn.lastName.sendKeys(selectAll,Keys.DELETE);
				wait.until(ExpectedConditions.elementToBeClickable(signIn.lastName));
				String lName = data.get(1).get(1);
				String LastName = signIn.randomNumber(lName);				
				signIn.lastName.sendKeys(LastName);
				
				signIn.phoneNumber.click();
				signIn.phoneNumber.sendKeys(selectAll,Keys.DELETE);
				wait.until(ExpectedConditions.elementToBeClickable(signIn.phoneNumber));
				String phoneNumber = data.get(2).get(1);
				signIn.phoneNumber.sendKeys(phoneNumber);
//				String PhoneNumber = signIn.randomNumber(phoneNumber);				
//				signIn.phoneNumber.sendKeys(PhoneNumber);
				
				
				wait.until(ExpectedConditions.elementToBeClickable(signIn.updateProfile));
				signIn.updateProfile.click();
				Thread.sleep(2000);
				String profileInfo = signIn.profileInfo.getText();
				String profileFirstName = profileInfo.substring(0, 13);
				Assert.assertEquals(FirstName, profileFirstName);
				
				
				
		}
	    @And("^Click on \"view all\" link in Address Book section$")
		public void click_on_view_all_button() throws Throwable {						
				wait.until(ExpectedConditions.elementToBeClickable(signIn.viewAllAddress));
				signIn.viewAllAddress.click();
		}
	    
	    @And("^Click on \"New Address\" link$")
		public void click_on_new_Address_button() throws Throwable {						
				wait.until(ExpectedConditions.elementToBeClickable(signIn.newAddress));
				signIn.newAddress.click();
		}

	    
	    @And("^In the opened dialog, enter valid inputs in all the fields.$")
		public void enter_the_requiredDetails(DataTable table) throws Throwable {
	    		List<List<String>> data = table.raw();
				wait.until(ExpectedConditions.elementToBeClickable(signIn.firstName_forNewAddress));
				String fName = data.get(0).get(1);
				String FirstName = signIn.randomNumber(fName);				
				signIn.firstName_forNewAddress.sendKeys(FirstName);
				
				String lName = data.get(1).get(1);
				String LastName = signIn.randomNumber(lName);				
				signIn.lastName_forNewAddress.sendKeys(LastName);
				
				String nName = data.get(2).get(1);
				String NickName = signIn.randomNumber(nName);				
				signIn.nickName_forNewAddress.sendKeys(NickName);
				
				String address = data.get(3).get(1);						
				signIn.addNewAddressInputField.sendKeys(address);
				signIn.addNewAddressInputField.click();
				Thread.sleep(4000);
				signIn.addNewAddressInputField.sendKeys(Keys.DOWN);
				signIn.addNewAddressInputField.sendKeys(Keys.ENTER);
				
				
		}
	    
	    @And("^Click on Save button and verify the new address got added successfully$")
		public void click_on_saveBtn_and_verify() throws Throwable {						
				wait.until(ExpectedConditions.elementToBeClickable(signIn.saveNewAddressBtn));
				Thread.sleep(2000);
				signIn.saveNewAddressBtn.click();
				wait.until(ExpectedConditions.visibilityOf(order.growlMessage));
				Assert.assertEquals("The address has been saved successfully",order.growlMessage.getText());
				wait.until(ExpectedConditions.elementToBeClickable(signIn.deleteAddress));
				signIn.deleteAddress.click();
				Thread.sleep(1000);
				driver.switchTo().alert().accept();
		}	
	    
	    
	    @And("^Click on \"Edit Address\" link$")
		public void click_on_edit_Address() throws Throwable {						
				wait.until(ExpectedConditions.elementToBeClickable(signIn.editAddress));
				signIn.editAddress.click();
		}
	    
	    @And("^change the address and click on save button and verify the address is updated successfully$")
		public void update_Address(DataTable table) throws Throwable {
	    		List<List<String>> data = table.raw();
				wait.until(ExpectedConditions.elementToBeClickable(signIn.editAddressInput));	
				
				String address = data.get(0).get(1);						
				signIn.editAddressInput.sendKeys(address);
				signIn.editAddressInput.click();
				Thread.sleep(4000);
				signIn.editAddressInput.sendKeys(Keys.DOWN);
				signIn.editAddressInput.sendKeys(Keys.ENTER);
				
				wait.until(ExpectedConditions.elementToBeClickable(signIn.updateAddress));
				Thread.sleep(2000);
				signIn.updateAddress.click();
				wait.until(ExpectedConditions.visibilityOf(order.growlMessage));
				Assert.assertEquals("The address has been saved successfully",order.growlMessage.getText());
				wait.until(ExpectedConditions.elementToBeClickable(signIn.deleteAddress));
				signIn.deleteAddress.click();
				Thread.sleep(1000);
				driver.switchTo().alert().accept();
				
		}
	    
	    
	    @And("^Click on Save button and after saving delete the address$")
		public void delete_the_address() throws Throwable {						
				wait.until(ExpectedConditions.elementToBeClickable(signIn.saveNewAddressBtn));
				Thread.sleep(2000);
				signIn.saveNewAddressBtn.click();
				wait.until(ExpectedConditions.visibilityOf(order.growlMessage));
				Assert.assertEquals("The address has been saved successfully",order.growlMessage.getText());
				wait.until(ExpectedConditions.elementToBeClickable(signIn.deleteAddress));
				signIn.deleteAddress.click();
				Thread.sleep(1000);
				driver.switchTo().alert().accept();
		}	
	    
	    @And("^Click on Checkout Button$")
		public void click_checkoutBtn() throws Throwable {						
				wait.until(ExpectedConditions.visibilityOf(signIn.myAccountCheckoutBtn));
				signIn.myAccountCheckoutBtn.click();
				
		}
	    
	    @Then("^verify saved card info is displayed$")
		public void verify_saved_card_info() throws Throwable {
			wait.until(ExpectedConditions.visibilityOf(signIn.cardInfo));
			String ccInfo = signIn.cardInfo.getText();
			if(ccInfo.contains("1111")) {
				System.out.println("Credit Card info is displayed"+ccInfo);
			}
			else {
				System.out.println("credit card info not displayed");
			}			
			
		}
	    
		
		@And("^Click on \"view all\" in Credit cards section.$")
		public void click_on_ViewAll_CC_section() throws Throwable {
			wait.until(ExpectedConditions.visibilityOf(signIn.viewAllCC));
			signIn.viewAllCC.click();
			
		}
	    
	    @Then("^Click \"delete\" present in front of credit cards displayed and verify CC deleted successfully$")
		public void click_delete() throws Throwable {
			wait.until(ExpectedConditions.visibilityOf(signIn.deleteCC));
			signIn.deleteCC.click();
			Thread.sleep(2000);
			driver.switchTo().alert().accept();

			wait.until(ExpectedConditions.visibilityOf(order.growlMessage));
			Assert.assertEquals("Credit card deleted successfully", order.growlMessage.getText());  
		}
	    
	    
	    @Then("^verify the email textbox contains the username entered at the time of signup$")
		public void verify_the_email() throws Throwable {
			wait.until(ExpectedConditions.visibilityOf(signIn.signedInEmail));			
			Thread.sleep(8000);
			String str = signIn.signedInEmail.getAttribute("value");			
			Assert.assertEquals("icprateeknehra@gmail.com", str);  
		}
	    
	    @Then("^verify no CC is saved and the message is displayed$")
		public void verify_message_is_displayed() throws Throwable {
			wait.until(ExpectedConditions.visibilityOf(signIn.noCCText));			
			
			Assert.assertEquals("You do not have any saved credit cards.", signIn.noCCText.getText());  
		}
	    
	    @Then("^verify the email textbox is disabled$")
		public void verify_email_textbox_is_disabled() throws Throwable {
			wait.until(ExpectedConditions.visibilityOf(signIn.signedInEmail));			
			Boolean isReadOnlyTrue = signIn.isreadOnly(signIn.signedInEmail);
			Assert.assertTrue(isReadOnlyTrue);
			
		}
	    
	    
	    @Then("^Update FirstName and click Update Profile button and verify updated firstname is displayed on top right of the profile page$")
		public void update_FirstName(DataTable table) throws Throwable {	
	    	
	    		List<List<String>> data = table.raw();
				wait.until(ExpectedConditions.elementToBeClickable(signIn.firstName));
				Thread.sleep(5000);
//				signIn.firstName.click();
				String selectAll = Keys.chord(Keys.CONTROL, "a");				
				signIn.firstName.sendKeys(selectAll,Keys.DELETE);
				wait.until(ExpectedConditions.elementToBeClickable(signIn.firstName));
				String fName = data.get(0).get(1);
				String FirstName = signIn.randomNumber(fName);				
				signIn.firstName.sendKeys(FirstName);
				
				wait.until(ExpectedConditions.elementToBeClickable(signIn.updateProfile));
				signIn.updateProfile.click();
				Thread.sleep(2000);
				String profileInfo = signIn.profileInfo.getText();
				String profileFirstName = profileInfo.substring(0, 13);
				
				String topProfileInfo = signIn.topProfileName.getText();
				String topProfileFirstName = topProfileInfo.substring(4, 17);
				System.out.println(topProfileFirstName);
				Assert.assertEquals(profileFirstName, topProfileFirstName);								
				
		}
	    

	    @And("^Enter Current Password$")
		public void enter_current_password(DataTable table) throws Throwable {
	    	List<List<String>> data = table.raw();
	    	Thread.sleep(3000);
			wait.until(ExpectedConditions.visibilityOf(signIn.currentPassword));			
			signIn.currentPassword.sendKeys(data.get(0).get(1));			
			
		}
	    
	    @And("^Enter New Password$")
		public void enter_new_password(DataTable table) throws Throwable {
	    	List<List<String>> data = table.raw();
			wait.until(ExpectedConditions.visibilityOf(signIn.newPassword));
			signIn.newPassword.click();
			signIn.newPassword.sendKeys(data.get(0).get(1));
			
		}
	    
	    @And("^Enter Confirm Password$")
		public void enter_confirm_password(DataTable table) throws Throwable {
	    	List<List<String>> data = table.raw();
			wait.until(ExpectedConditions.visibilityOf(signIn.confirmPassword));
			signIn.confirmPassword.click();
			signIn.confirmPassword.sendKeys(data.get(0).get(1));		
			
		}
	    
	    
	    @Then("^click update password and verify password changed successfully and change the password back to original$")
		public void click_update_Password(DataTable table) throws Throwable {	
	    	List<List<String>> data = table.raw();
			wait.until(ExpectedConditions.visibilityOf(signIn.updatePasswordBtn));
			signIn.updatePasswordBtn.click();
			Assert.assertEquals("Password updated successfully",order.growlMessage.getText());
			System.out.println("+++++++++++Password update Success++++++++++++");
			
			wait.until(ExpectedConditions.visibilityOf(signIn.currentPassword));
			signIn.currentPassword.click();
			signIn.currentPassword.sendKeys(data.get(0).get(1));
			wait.until(ExpectedConditions.visibilityOf(signIn.newPassword));	
			signIn.newPassword.click();
			signIn.newPassword.sendKeys(data.get(1).get(1));
			wait.until(ExpectedConditions.visibilityOf(signIn.confirmPassword));
			signIn.confirmPassword.click();
			signIn.confirmPassword.sendKeys(data.get(2).get(1));
			Thread.sleep(1000);
			signIn.updatePasswordBtn.click();
		}
	    
	     
	    @Then("^verify password entered should be hidden in New password box$")
		public void entered_password_isHidden() throws Throwable {	    	
			wait.until(ExpectedConditions.visibilityOf(signIn.newPasswordType));
			String actualAttribute = signIn.newPasswordType.getAttribute("type");			
			if (actualAttribute.equals("password")){
			    System.out.println("Password is masked");
			}else {
			    Assert.fail("+++++++Test Fail++++++++++++Password is not masked");
			}		
			
		}
	    
	    
	    @Then("^verify password entered should be hidden in current password box$")
		public void entered_Currentpassword_isHidden() throws Throwable {	    	
			wait.until(ExpectedConditions.visibilityOf(signIn.currentPassword));
			String actualAttribute = signIn.currentPassword.getAttribute("type");			
			if (actualAttribute.equals("password")){
			    System.out.println("Password is masked");
			}else {
			    Assert.fail("+++++++Test Fail++++++++++++Password is not masked");
			}		
			
		}
	    
	    
	    @Then("^verify password entered should be hidden in Confirm password box$")
		public void entered_Confirmpassword_isHidden() throws Throwable {	    	
			wait.until(ExpectedConditions.visibilityOf(signIn.confirmPassword));
			String actualAttribute = signIn.confirmPassword.getAttribute("type");			
			if (actualAttribute.equals("password")){
			    System.out.println("Password is masked");
			}else {
			    Assert.fail("+++++++Test Fail++++++++++++Password is not masked");
			}		
			
		}
	    
	    
	    @Then("^Click Update Password and Verify pop-up should be displayed saying The current password field is required$")
		public void popUp_CurrentPasswordMissing() throws Throwable {	
	    	wait.until(ExpectedConditions.visibilityOf(signIn.updatePasswordBtn));
	    	signIn.updatePasswordBtn.click();
			wait.until(ExpectedConditions.visibilityOf(order.growlMessage));
				
			Assert.assertEquals("The current password field is required.",order.growlMessage.getText());
		}
	    
	    @Then("^Click Update Password and Verify pop-up should be displayed saying The New password field is required$")
		public void popUp_NewPasswordMissing() throws Throwable {	
	    	wait.until(ExpectedConditions.visibilityOf(signIn.updatePasswordBtn));
	    	signIn.updatePasswordBtn.click();
			wait.until(ExpectedConditions.visibilityOf(order.growlMessage));
				
			Assert.assertEquals("The new password field is required.",order.growlMessage.getText());
		}
	    
	    @Then("^Click Update Password and Verify pop-up should be displayed saying The Confirm password field is required$")
		public void popUp_ConfirmPasswordMissing() throws Throwable {	
	    	wait.until(ExpectedConditions.visibilityOf(signIn.updatePasswordBtn));
	    	signIn.updatePasswordBtn.click();
			wait.until(ExpectedConditions.visibilityOf(order.growlMessage));
				
			Assert.assertEquals("The new password confirmation field is required.",order.growlMessage.getText());			
			
		}
	    
	    @Then("^Click Update Password and Verify pop-up should be displayed stating The new password confirmation and new password must match$")
		public void popUp_newPasswordandConfirmPassword_ShouldMatch() throws Throwable {	
	    	wait.until(ExpectedConditions.visibilityOf(signIn.updatePasswordBtn));
	    	signIn.updatePasswordBtn.click();
			wait.until(ExpectedConditions.visibilityOf(order.growlMessage));
				
			Assert.assertEquals("The new password confirmation and new password must match.",order.growlMessage.getText());			
			
		}
	    
	    
	    @Then("^Click Update Password and Verify pop-up should be displayed stating The current password entered does not match the password on your account$")
		public void popUp_incorrectCurrent_password() throws Throwable {	
	    	wait.until(ExpectedConditions.visibilityOf(signIn.updatePasswordBtn));
	    	signIn.updatePasswordBtn.click();
			wait.until(ExpectedConditions.visibilityOf(order.growlMessage));
				
			Assert.assertEquals("The current password entered does not match the password on your account.",order.growlMessage.getText());			
			
		}
	    
	    
	    @Then("^Click Update Password and Verify pop-up should be displayed stating The new password and current password must be different$")
		public void popUp_sameNewandCurrent_password() throws Throwable {	
	    	wait.until(ExpectedConditions.visibilityOf(signIn.updatePasswordBtn));
	    	signIn.updatePasswordBtn.click();
			wait.until(ExpectedConditions.visibilityOf(order.growlMessage));
				
			Assert.assertEquals("The new password and current password must be different.",order.growlMessage.getText());			
			
		}
	    
	    
	    @Then("^Click Update Password and Verify pop-up should be displayed stating The new password must be at least 5 characters.$")
		public void popUp_password_mustbe_more_than_5_characters() throws Throwable {	
	    	wait.until(ExpectedConditions.visibilityOf(signIn.updatePasswordBtn));
	    	signIn.updatePasswordBtn.click();
			wait.until(ExpectedConditions.visibilityOf(order.growlMessage));
				
			Assert.assertEquals("The new password must be at least 5 characters.",order.growlMessage.getText());			
			
		}
	    
	    
	    @Then("^Verify by default Allow push notifications checkbox is checked$")
		public void verify_checkBox_isChecked() throws Throwable {
	    	Thread.sleep(4000);
//	    	wait.until(ExpectedConditions.visibilityOf(signIn.allowPushNotificationCheckBox));	    	
			Assert.assertTrue("++++++The allow Push Notification checkbox is not checked by default+++++", signIn.allowPushNotificationCheckBox.isSelected());
			
		}
	    
	    @Then("^Uncheck and verify the Allow push notifications checkbox is unchecked$")
		public void verify_checkBox_isUnChecked() throws Throwable {
	    	Thread.sleep(1000);
//	    	wait.until(ExpectedConditions.visibilityOf(signIn.allowPushNotificationCheckBox));
	    	signIn.allowPushNotificationCheckBox.click();
			Assert.assertFalse("++++++The allow Push Notification checkbox is not getting unchecked+++++", signIn.allowPushNotificationCheckBox.isSelected());
			
		}
	    
	    @Then("^Verify user is able to check/uncheck Remember Credit Cards check box$")
		public void verify_CCcheckBox_isChecked_andUnchecked() throws Throwable {
	    	Thread.sleep(4000);
//	    	wait.until(ExpectedConditions.visibilityOf(signIn.allowPushNotificationCheckBox));
	    	signIn.remeberCC_ChkBox.click();
	    	Assert.assertTrue("++++++The Remeber Credit Card checkbox is not getting checkedt+++++", signIn.remeberCC_ChkBox.isSelected());			
			signIn.remeberCC_ChkBox.click();
			Assert.assertFalse("++++++The Remeber Credit Card checkbox is not getting unchecked+++++", signIn.remeberCC_ChkBox.isSelected());
			
		}
	    
	    @Then("^Verify user is able to check/uncheck Are you responsible for ordering for large groups check box$")
		public void verify_largeGrp_isChecked_andUnchecked() throws Throwable {
	    	Thread.sleep(4000);
//	    	wait.until(ExpectedConditions.visibilityOf(signIn.allowPushNotificationCheckBox));
	    	signIn.largeGrpChkBox.click();
	    	Assert.assertTrue("++++++The Remeber Credit Card checkbox is not getting checkedt+++++", signIn.largeGrpChkBox.isSelected());			
			signIn.largeGrpChkBox.click();
			Assert.assertFalse("++++++The Remeber Credit Card checkbox is not getting unchecked+++++", signIn.largeGrpChkBox.isSelected());
			
		}
	    
	    @Then("^Verify user is able to check/uncheck event offerings check box$")
		public void verify_eventOfferings_isChecked_andUnchecked() throws Throwable {
	    	Thread.sleep(4000);
//	    	wait.until(ExpectedConditions.visibilityOf(signIn.allowPushNotificationCheckBox));
	    	signIn.eventOfferingsChkBox.click();
	    	Assert.assertTrue("++++++The Remeber Credit Card checkbox is not getting checkedt+++++", signIn.eventOfferingsChkBox.isSelected());			
			signIn.eventOfferingsChkBox.click();
			Assert.assertFalse("++++++The Remeber Credit Card checkbox is not getting unchecked+++++", signIn.eventOfferingsChkBox.isSelected());
			
		}
	    
	    @Then("^Verify user is able to check/uncheck weekly updates check box$")
		public void verify_weeklyUpdate_isChecked_andUnchecked() throws Throwable {
	    	Thread.sleep(4000);
//	    	wait.until(ExpectedConditions.visibilityOf(signIn.allowPushNotificationCheckBox));
	    	signIn.newsLetterChkBox.click();
	    	Assert.assertFalse("++++++The Remeber Credit Card checkbox is not getting unchecked+++++", signIn.newsLetterChkBox.isSelected());	    				
			signIn.newsLetterChkBox.click();
			Assert.assertTrue("++++++The Remeber Credit Card checkbox is not getting checkedt+++++", signIn.newsLetterChkBox.isSelected());
			
		}
	    
	    @Then("^Verify phone number more than 10 digits is not allowed$")
	    
		public void verify_phoneNumber_moreThan_10Digits_notAllowed(DataTable table) throws Throwable {	
	    	
	    		List<List<String>> data = table.raw();
				wait.until(ExpectedConditions.elementToBeClickable(signIn.firstName));
				Thread.sleep(5000);
//				signIn.firstName.click();
				String selectAll = Keys.chord(Keys.CONTROL, "a");				
								
				signIn.phoneNumber.click();
				signIn.phoneNumber.sendKeys(selectAll,Keys.DELETE);
				wait.until(ExpectedConditions.elementToBeClickable(signIn.phoneNumber));
				String phoneNumber = data.get(0).get(1);				
				signIn.phoneNumber.sendKeys(phoneNumber);
				Thread.sleep(1000);
				System.out.println("Print Phone Number"+phoneNumber);

				long actualPhoneNumber = Long.parseLong(phoneNumber);
//				String enteredPhoneNumber = signIn.phoneNumber.getText();
				String enteredPhoneNumber = signIn.phoneNumber.getAttribute("value");
				System.out.println("Entered phone number is+++"+enteredPhoneNumber);
				long enteredPhNumber = Long.parseLong(enteredPhoneNumber);
				if(enteredPhNumber != actualPhoneNumber) {
					System.out.println("++++++++++++Phone Number more than 10 digits is not allowed");
					
				}else {
					Assert.fail("+++++++++++Test Fail+++++++++++++Phone Number more than 10 digits is getting allowed");
				}
				

		}
	    
 @Then("^Verify phone number less than 10 digits is not allowed$")
	    
		public void verify_phoneNumber_lessThan_10Digits_notAllowed(DataTable table) throws Throwable {	
	    	
	    		List<List<String>> data = table.raw();
				wait.until(ExpectedConditions.elementToBeClickable(signIn.firstName));
				Thread.sleep(5000);
//				signIn.firstName.click();
				String selectAll = Keys.chord(Keys.CONTROL, "a");				
								
				signIn.phoneNumber.click();
				signIn.phoneNumber.sendKeys(selectAll,Keys.DELETE);
				wait.until(ExpectedConditions.elementToBeClickable(signIn.phoneNumber));
				String phoneNumber = data.get(0).get(1);				
				signIn.phoneNumber.sendKeys(phoneNumber);
				wait.until(ExpectedConditions.elementToBeClickable(signIn.updateProfile));
				signIn.updateProfile.click();
				wait.until(ExpectedConditions.elementToBeClickable(order.growlMessage));
				String msg = order.growlMessage.getText();
				Assert.assertEquals("The telephone must be 10 digits.", msg);
				

		}
 
 @And("^Click the logout button$")
 
	public void click_LogoutBtn() throws Throwable {	

			wait.until(ExpectedConditions.elementToBeClickable(signIn.logout));
			Thread.sleep(3000);
			signIn.logout.click();

	}
 
 @Then("^verify user logsout successfully$")
 
	public void verify_UserLogsOutSuccessfully() throws Throwable {	

			wait.until(ExpectedConditions.elementToBeClickable(signIn.login_button));
			if(signIn.login_button.isDisplayed()) {
				System.out.println("User Logged out Success ++++++++++++");
				
			}else {
				Assert.fail("User unable to logout+++++++++++++");
			}

	}
 
 
 @Then("^In the opened dialog, validate FirstName, LastName, NickName, Address, Apt/Suites, Dorm$")
	public void validate_AddressInfo(DataTable table) throws Throwable {
 		List<List<String>> data = table.raw();
			wait.until(ExpectedConditions.elementToBeClickable(signIn.firstName_forEditAddress));
			String fName = data.get(0).get(1);
			String enteredFirstName = signIn.firstName_forEditAddress.getAttribute("value");				
			Assert.assertEquals("FirstName entered is not correct", fName, enteredFirstName);
			
			wait.until(ExpectedConditions.elementToBeClickable(signIn.lastName_forEditAddress));
			String lName = data.get(1).get(1);
			String enteredLastName = signIn.lastName_forEditAddress.getAttribute("value");				
			Assert.assertEquals("LastName entered is not correct", lName, enteredLastName);
			
			wait.until(ExpectedConditions.elementToBeClickable(signIn.nickName_forEditAddress));
			String nickName = data.get(2).get(1);
			String enterednickName = signIn.nickName_forEditAddress.getAttribute("value");				
			Assert.assertEquals("NickName entered is not correct", nickName, enterednickName);
			
			wait.until(ExpectedConditions.elementToBeClickable(signIn.address_forEditAddress));
			String address = data.get(3).get(1);
			String enteredAddress = signIn.address_forEditAddress.getAttribute("placeholder");				
			Assert.assertEquals("Address entered is not correct", address, enteredAddress);
			
			wait.until(ExpectedConditions.elementToBeClickable(signIn.aptSuite_forEditAddress));
			String aptSuite = data.get(4).get(1);
			String enteredaptSuite = signIn.aptSuite_forEditAddress.getAttribute("value");	
			
			Assert.assertEquals("AptSuiteName entered is not correct", aptSuite, enteredaptSuite);
			Thread.sleep(3000);
			
			Select select = new Select(signIn.dormName_forEditAddress);
			WebElement selectedOption = select.getFirstSelectedOption();
			String enteredDorm = selectedOption.getText();
			String dorm = data.get(5).get(1);
			Assert.assertEquals("DormName entered is not correct", dorm, enteredDorm);
			
			
	}
 
 	@Then("^verify by clicking star the address becomes favorite$")
 	public void validate_Favorite_Address() throws Throwable {
 		Thread.sleep(2000);
 		List <WebElement> list =  signIn.favoriteList;
 		WebElement secondFavorite = list.get(1);
 		secondFavorite.click();
 		wait.until(ExpectedConditions.visibilityOf(order.growlMessage));
 		String favAddSuccessMsg = order.growlMessage.getText();
 		Assert.assertEquals("Address not added as favorite", "Your default address has been changed", favAddSuccessMsg);
 		Thread.sleep(500);
 		secondFavorite.click();
 	}
 	
 	@Then("^verify by clicking star the address moves to top$")
 	public void validate_FavAddress_movesOnTop() throws Throwable {
 		Thread.sleep(2000);
 		List <WebElement> list =  signIn.favoriteList;
 		WebElement secondFavorite = list.get(1);
 		secondFavorite.click();
 		wait.until(ExpectedConditions.visibilityOf(order.growlMessage));
 		String favAddSuccessMsg = order.growlMessage.getText();
 		Assert.assertEquals("Address not added as favorite", "Your default address has been changed", favAddSuccessMsg);
 		String favAddressName = signIn.favAddressName.getText();
 		Assert.assertEquals("FavAddress dosen't moved on top test fail++++", " TestName Test", favAddressName);
 		Thread.sleep(500);
 		secondFavorite.click();
 	}
 	
 	@Then("^verify favourite address is displayed under Address book section$")
 	public void validate_FavAddress_isDisplayed_underAddress_book() throws Throwable {
 		wait.until(ExpectedConditions.visibilityOf(signIn.addressDetails));
 		String addressUnderAddressbook = signIn.addressDetails.getText();
 		String favAddressonTop = "1084 East Lancaster Avenue Test Suites";
 		System.out.println(addressUnderAddressbook);
 		System.out.println(favAddressonTop);
 		if(addressUnderAddressbook.contains(favAddressonTop)) {
 			System.out.println("Fav address is shown under Address Book");
 		}else {
 			Assert.fail("Test Fail :::::::::::Fav address is not shown under Address Book ");
 		}
		
 	}
 	
 	@Then("^verify by clicking star pop up appears stating Your default address had been changed$")
 	public void verify_popup_displayed() throws Throwable {
 		Thread.sleep(2000);
 		List <WebElement> list =  signIn.favoriteList;
 		WebElement secondFavorite = list.get(1);
 		secondFavorite.click();
 		wait.until(ExpectedConditions.visibilityOf(order.growlMessage));
 		String favAddSuccessMsg = order.growlMessage.getText();
 		Assert.assertEquals("Address not added as favorite", "Your default address has been changed", favAddSuccessMsg);
 		Thread.sleep(500);
 		secondFavorite.click();
 	}
	    
}
