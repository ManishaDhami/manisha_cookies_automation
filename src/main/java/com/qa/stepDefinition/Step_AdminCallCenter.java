package com.qa.stepDefinition;

import java.util.List;
import java.util.Random;

import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.qa.pages.AdminCallCenter;
import com.qa.pages.AdminStores;
import com.qa.utils.TestBase;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Step_AdminCallCenter extends TestBase{
	
	WebDriver driver = getDriver();
	AdminStores adminStores = new AdminStores(driver);
	AdminCallCenter adminCallCenter = new AdminCallCenter(driver);
	WebDriverWait wait = new WebDriverWait(driver, 20);
	String admin_url = getAdminUrl();
	JavascriptExecutor jse = (JavascriptExecutor)driver;
	Actions action =  new Actions(driver);
	
	
	
	@Given("^Verify if user is logged in$")
	public void verify_if_user_is_logged_in() throws Throwable {
		adminStores.Admin_login();
	}

	@When("^user clicks CallCenter$")
	public void user_clicks_CallCenter() throws Throwable {
		Thread.sleep(4000);
		wait.until(ExpectedConditions.visibilityOf(adminCallCenter.callCenter));
		adminCallCenter.callCenter.click();
		Thread.sleep(1000);
		if(adminCallCenter.isAlertPresent(driver)){
			  Alert alert = driver.switchTo().alert();
			  System.out.println(alert.getText());
			  alert.accept();
			 }
		}

	@When("^user enters FirstName$")
	public void user_enters_FirstName(DataTable dt) throws Throwable {
		List<List<String>> list = dt.raw();
		try{			 
		wait.until(ExpectedConditions.elementToBeClickable(adminCallCenter.firstName));
		adminCallCenter.firstName.sendKeys(list.get(0).get(0));
		}
		catch (Exception e){
			e.printStackTrace();
			Alert alert = driver.switchTo().alert();
			alert.accept();
			wait.until(ExpectedConditions.visibilityOf(adminCallCenter.firstName));
			adminCallCenter.firstName.sendKeys(list.get(0).get(0));
		}
		
	}

	@When("^User enters LastName$")
	public void user_enters_LastName(DataTable dt) throws Throwable {
		List<List<String>> list = dt.raw();
		adminCallCenter.lastName.sendKeys(list.get(0).get(0));
	}

	@When("^user enters PhoneNumber$")
	public void user_enters_PhoneNumber(DataTable dt) throws Throwable {
		List<List<String>> list = dt.raw();
		adminCallCenter.phoneNumber.click();
	   adminCallCenter.phoneNumber.sendKeys(list.get(0).get(0));
	   
	   
	}

	@When("^user enters email$")
	public void user_enters_email(DataTable dt) throws Throwable {
		List<List<String>> list = dt.raw();
//	    adminCallCenter.email.sendKeys(list.get(0).get(0));
	    
		Random rand = new Random();
		int randomNumber = rand.nextInt(10000);
		randomNumber += 1;
		String randomStr = Integer.toString(randomNumber);
		String email = list.get(0).get(0);
		String randomEmail = randomStr+email;
		adminCallCenter.email.sendKeys(randomEmail);
	}

	@When("^user clicks on Continue Button$")
	public void user_clicks_on_Continue_Button() throws Throwable {
		Thread.sleep(500);
	    adminCallCenter.continueCustTab.click();
	}

	@Then("^Verify Shipping tab is open$")
	public void verify_Shipping_tab_is_open() throws Throwable {
		wait.until(ExpectedConditions.visibilityOf(adminCallCenter.shipFirstName));
	Assert.assertTrue("Shipping tab open failure:#########", adminCallCenter.shipFirstName.isDisplayed());
	    
	}

	@When("^user Enters the address and click Find Stores$")
	public void user_Enters_the_address(DataTable dt) throws Throwable {
		List<List<String>> list = dt.raw();
		wait.until(ExpectedConditions.visibilityOf(adminCallCenter.address));
	    adminCallCenter.address.sendKeys(list.get(0).get(0));
	    Thread.sleep(1000);
	    adminCallCenter.address.sendKeys(Keys.DOWN, Keys.RETURN);
//	    Select select = new Select(adminCallCenter.state);
//	    select.selectByVisibleText("MS");
//		Thread.sleep(1000);
	    adminCallCenter.zip.click();
	    Thread.sleep(5000);
	    adminCallCenter.findStore.click();
	    Thread.sleep(3000);
	    
	}

	@When("^user click Delivery Button$")
	public void user_click_Delivery_Button() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(adminCallCenter.deliveryBtn));
	    adminCallCenter.deliveryBtn.click();
	    Thread.sleep(2000);
	}

	@When("^user select Off Campus$")
	public void user_select_Off_Campus() throws Throwable {
//		jse.executeScript("arguments[0].click()", adminCallCenter.offCampusBtn);
		wait.until(ExpectedConditions.elementToBeClickable(adminCallCenter.offCampusBtn));
	    adminCallCenter.offCampusBtn.click();
	    Thread.sleep(2000);
	}

	@When("^click Continue Delivery button$")
	public void click_Continue_Delivery_buttonm() throws Throwable {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOf(adminCallCenter.continueDeliveryBtn));
		jse.executeScript("arguments[0].click()", adminCallCenter.continueDeliveryBtn);
		//adminCallCenter.continueDeliveryBtn.click();
		Thread.sleep(5000);
		try{
			if(adminCallCenter.closeTabMsg.getText().equals("Please close the tab and return to your order.")){
				//driver.navigate().refresh();
				driver.navigate().to(driver.getCurrentUrl()); 
				Thread.sleep(1000);
				if(adminCallCenter.isAlertPresent(driver)){
					  Alert alert = driver.switchTo().alert();
					  System.out.println(alert.getText());
					  alert.accept();
						if(adminCallCenter.isAlertPresent(driver)){
							  System.out.println(alert.getText());
							  alert.accept();
							 }				  
					 }
			}

			}catch (Exception e){
			e.printStackTrace();
			System.out.println("Close Tab Msg didn't poped up");
		}
	}

	@Then("^verify Order Tab is open$")
	public void verify_Order_Tab_is_open() throws Throwable {
		WebDriverWait waitLocal = new WebDriverWait(driver, 35);
		waitLocal.until(ExpectedConditions.visibilityOf(adminCallCenter.cart));
		Assert.assertTrue("Order tab open failure:#########", adminCallCenter.cart.isDisplayed());
	}

	@When("^user Adds an Item$")
	public void user_Adds_an_Item() throws Throwable {
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOf(adminCallCenter.sixPack));
	    adminCallCenter.sixPack.click();
	    wait.until(ExpectedConditions.visibilityOf(adminCallCenter.assortment));
	    adminCallCenter.assortment.click();
	    wait.until(ExpectedConditions.visibilityOf(adminCallCenter.addBtn));
	    adminCallCenter.addBtn.click();
	}
	
	@And("^user Adds another Item$")
	public void user_Adds_Another_Item() throws InterruptedException{
		wait.until(ExpectedConditions.visibilityOf(adminCallCenter.twoPercentMilk));
		Thread.sleep(2000);
		adminCallCenter.twoPercentMilk.click();
	    wait.until(ExpectedConditions.visibilityOf(adminCallCenter.addBtn));
	    adminCallCenter.addBtn.click();
	}

	@When("^user selects delivery date as tomorrow$")
	public void user_selects_delivery_date_as_tomorrow() throws Throwable {
		 Thread.sleep(1000);
//		 jse.executeScript("arguments[0].scrollIntoView(true);", adminCallCenter.orderDate);
		 jse.executeScript("window.scrollBy(0,650)");		 
		 wait.until(ExpectedConditions.elementToBeClickable(adminCallCenter.orderDate));
//		 adminCallCenter.asap.click();

		 adminCallCenter.orderDate.click();
	        Thread.sleep(2000);


	        String tomorrow = adminCallCenter.getCurrentDay();
	        System.out.println("Today's number: " + tomorrow + "\n");
	        List<WebElement> columns = adminCallCenter.calender.findElements(By.tagName("td"));

	        for (WebElement cell : columns) {
	            if (cell.getText().equals(tomorrow)) {
	                cell.click();
	                break;
	            }
	        }

	}
    
	@And ("^user selects delivery date as 6 months from today$")
	public void user_selects_delivery_date_as_six_months_from_today() throws Throwable {
		 Thread.sleep(1000);
		 jse.executeScript("window.scrollBy(0,650)");		 
		 wait.until(ExpectedConditions.elementToBeClickable(adminCallCenter.orderDate));

		 adminCallCenter.orderDate.click();
	        Thread.sleep(2000);
	     for(int i = 0;i<6;i++)
	     {
	    	adminCallCenter.calenderNextBtn.click();
	    	wait.until(ExpectedConditions.elementToBeClickable(adminCallCenter.calenderNextBtn));
	     }

	        String tomorrow = adminCallCenter.getCurrentDay();
	        int intTomorrow = Integer.parseInt(tomorrow);
	        int intToday = intTomorrow - 1 ;
	        String strToday = Integer.toString(intToday);
	        
	        System.out.println("Today's number: " + strToday + "\n");
	        List<WebElement> columns = adminCallCenter.calender.findElements(By.tagName("td"));

	        for (WebElement cell : columns) {
	            if (cell.getText().equals(strToday)) {
	                cell.click();
	                break;
	            }
	        }

	}
	
	
	@When("^user select the delivery time$")
	public void user_select_the_time() throws Throwable {
		
		Select time_dropdown = new Select(adminCallCenter.orderTimeDropDown);
		Thread.sleep(2000);
		time_dropdown.selectByIndex(4);
		Thread.sleep(2000);

	}

	@When("^User Clicks the checkout Button$")
	public void user_Clicks_the_checkout_Button() throws Throwable {
		//jse.executeScript("window.scrollBy(0,1500)");
		
		jse.executeScript("arguments[0].scrollIntoView(true);", adminCallCenter.checkOutBtn);
		Thread.sleep(2000); 
		action.moveToElement(adminCallCenter.checkOutBtn).build().perform();
		jse.executeScript("arguments[0].click()", adminCallCenter.checkOutBtn);
		//adminCallCenter.checkOutBtn.click();
		

	}

	@Then("^Verify Billing tab is open$")
	public void verify_Billing_tab_is_open() throws Throwable {
		WebDriverWait waitlocal = new WebDriverWait(driver, 35);
		waitlocal.until(ExpectedConditions.elementToBeClickable(adminCallCenter.editOrder));
		Assert.assertTrue("Billing tab open failure:#########", adminCallCenter.editOrder.isDisplayed());

	}

	@Then("^user selects Cash on Delivery as payment method$")
	public void user_selects_Cash_on_Delivery_as_payment_method() throws Throwable {

		List<WebElement> list = adminCallCenter.paymentMethod;
		list.get(1).click();
	}

	@Then("^Click Complete order$")
	public void click_Complete_order() throws Throwable {
		Thread.sleep(4000);
		WebDriverWait waitlocal = new WebDriverWait(driver, 15);

		waitlocal.until(ExpectedConditions.elementToBeClickable(adminCallCenter.completeOrder));
		Thread.sleep(2000);
//		adminCallCenter.completeOrder.click();
		jse.executeScript("arguments[0].click()", adminCallCenter.completeOrder);

	}

	@Then("^Click Complete order for School Cash$")
	public void click_Complete_order_For_School_Cash() throws Throwable {
		WebDriverWait waitlocal = new WebDriverWait(driver, 15);
		waitlocal.until(ExpectedConditions.elementToBeClickable(adminCallCenter.completeOrderSchoolCash));
		adminCallCenter.completeOrderSchoolCash.click();

	}
	
	@Then("^Verify Congrats message for successful Order Process$")
	public void verify_Conrats_message_for_successful_Order_Process() throws Throwable {
//		wait.until(ExpectedConditions.visibilityOf(adminCallCenter.orderSuccessMsg));
		Thread.sleep(2000);
		Assert.assertTrue("Order not placed successfully ###########", adminCallCenter.orderSuccessMsg.isDisplayed());
//		adminCallCenter.logout_AdminSite(driver);
	}
	
	@And("^user click PickUp Button$")
	public void user_Click_PickUp_Button() throws InterruptedException{
		Thread.sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(adminCallCenter.pickUpBtn));
		adminCallCenter.pickUpBtn.click();
		try{
		if(adminCallCenter.closeTabMsg.getText().equals("Please close the tab and return to your order.")){
			driver.navigate().refresh();
			Thread.sleep(1000);
			if(adminCallCenter.isAlertPresent(driver)){
				  Alert alert = driver.switchTo().alert();
				  System.out.println(alert.getText());
				  alert.accept();
					if(adminCallCenter.isAlertPresent(driver)){
						  System.out.println(alert.getText());
						  alert.accept();
						 }				  
				 }
		}

		}catch (Exception e){
			e.printStackTrace();
			System.out.println("Close Tab Msg didn't poped up##########################");
		}
		
	}
	
	@And("^user select On Campus$")
	public void user_Select_OnCampus() throws InterruptedException{
		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(adminCallCenter.onCampusBtn));
		adminCallCenter.onCampusBtn.click();
	}
	
	@And("^user select campus building$")
	public void user_Select_Campus_building(){
		wait.until(ExpectedConditions.elementToBeClickable(adminCallCenter.selCampusBuilding));
		Select selCampus = new Select(adminCallCenter.selCampusBuilding);
		selCampus.selectByIndex(4);
	}
	
	@And("^user clicks on Edit Order button$")
	public void user_clicks_Edit_Order(){
		wait.until(ExpectedConditions.elementToBeClickable(adminCallCenter.editOrder));
		adminCallCenter.editOrder.click();
	}
	
	@And("^user removes an item from cart$")
	public void user_removes_item_from_cart() throws InterruptedException{
		wait.until(ExpectedConditions.elementToBeClickable(adminCallCenter.removeItemFromCart));
		adminCallCenter.removeItemFromCart.click();
		Thread.sleep(1000);
		adminCallCenter.deleteProductYes.click();
		Thread.sleep(1000);
		
	}
	
	@When("^user selects Nova Cash as payment method$")
	public void user_select_Nova_Cash(){
		List<WebElement> list = adminCallCenter.paymentMethod;
		list.get(3).click();
	}
	
	@And("^user enters School Cash$")
	public void user_enters_school_cash(DataTable dt){
		wait.until(ExpectedConditions.visibilityOf(adminCallCenter.schoolCash));
		List<List<String>> list = dt.raw();
		adminCallCenter.schoolCash.sendKeys(list.get(0).get(0));
	}
	
	@And("^user Adds Cookie Cake to cart$")
	public void user_Add_Cookie_Cake(){
//		jse.executeScript("arguments[0].scrollIntoView(true);", adminCallCenter.cookieCake);
		jse.executeScript("window.scrollBy(0,1200)");	
		wait.until(ExpectedConditions.visibilityOf(adminCallCenter.cookieCake));
		adminCallCenter.cookieCake.click();
	    wait.until(ExpectedConditions.visibilityOf(adminCallCenter.addBtn));
	    adminCallCenter.addBtn.click();
		
	}
	
	@And("^user selects delivery date as today$")
	 public void user_Selects_DeliveryDate_as_Today() throws InterruptedException{
		
		 Thread.sleep(5000);
//		 jse.executeScript("arguments[0].scrollIntoView(true);", adminCallCenter.orderDate);
		 jse.executeScript("window.scrollBy(0,-400)");		 
		 wait.until(ExpectedConditions.elementToBeClickable(adminCallCenter.orderDate));
//		 adminCallCenter.asap.click();

		 adminCallCenter.orderDate.click();
	        Thread.sleep(2000);


	        String today = adminCallCenter.getToDay();
	        System.out.println("Today's number: " + today + "\n");
	        List<WebElement> columns = adminCallCenter.calender.findElements(By.tagName("td"));

	        for (WebElement cell : columns) {
	            if (cell.getText().equals(today)) {
	                cell.click();
	                break;
	            }
	        }

		
	}
	
	@And("^user select the delivery time as store open time$")
	public void user_select_store_open_time() throws InterruptedException{
		Select time_dropdown = new Select(adminCallCenter.orderTimeDropDown);
		Thread.sleep(2000);
		time_dropdown.selectByIndex(1);
		
	}
	
	@Then("^verify validation message stating Cake orders take longer to bake. is displayed$")
	public void verify_Validation_Message_is_Diaplayed(){
		wait.until(ExpectedConditions.visibilityOf(adminCallCenter.coockieCakeValidationMsg));
		Assert.assertTrue("Validation message for Cookie Cake can't be placed for pickup until 1 hr after the store opens is not displayed#################", 
				adminCallCenter.coockieCakeValidationMsg.isDisplayed());
	}
	
	@Then("^Verify call center module is open succesfully$")
	public void verify_CallCenter_Module_Is_Open() throws InterruptedException{
		wait.until(ExpectedConditions.visibilityOf(adminCallCenter.placeAnOrderValidationText));
		Assert.assertTrue("Call Center module didn't open ############", adminCallCenter.placeAnOrderValidationText.isDisplayed());		
				
//		adminCallCenter.logout_AdminSite(driver);
	}
	
	@When("^User Clicks Clear all button$")
	public void user_Clicks_ClearAll_Btn(){
		adminCallCenter.custTabClearAllBtn.click();
	}
	
	
	@Then("^Verify the customer form gets cleared$")
	public void verify_customer_form_gets_cleared(){
		String text = adminCallCenter.firstName.getAttribute("value");
		Assert.assertTrue("Clear All Button on Customer tab not working: #############", text.isEmpty());
			
//		adminCallCenter.logout_AdminSite(driver);
	}
	
	@When("^User Clicks Clear all button on shipping tab$")
	public void user_Clicks_ClearAll_Ship_Btn(){
		wait.until(ExpectedConditions.elementToBeClickable(adminCallCenter.shipTabClearAllBtn));
		adminCallCenter.shipTabClearAllBtn.click();
	}
	
	@Then("^Verify the Shipping form gets cleared$")
	public void verify_shipping_form_gets_cleared() throws InterruptedException{
		
		Thread.sleep(2000);
		String text = adminCallCenter.zip.getAttribute("value");
		Assert.assertTrue("Clear All Button on Shipping tab not working: #############", text.isEmpty());
		
//		adminCallCenter.logout_AdminSite(driver);
		
	}
	
	@And("^Click Cancel Order button$")
	public void click_Cancel_Order(){
		adminCallCenter.cancelOrder.click();
		wait.until(ExpectedConditions.elementToBeClickable(adminCallCenter.cancelOrderYesBtn));
		adminCallCenter.cancelOrderYesBtn.click();
		
	}
	
	@Then("^verify the order gets cancelled$")
	public void verify_Order_gets_Cancelled(){
		wait.until(ExpectedConditions.visibilityOf(adminCallCenter.placeAnOrderValidationText));
		Assert.assertTrue("Unable to Cancel Order: ##############", adminCallCenter.placeAnOrderValidationText.isDisplayed());
		
	}
	
	@Then("^verify the existing customer information gets displayed in Select Customer drop down list$")
	public void verify_existing_Cust_Info_Displayed(){
		wait.until(ExpectedConditions.visibilityOf(adminCallCenter.existingCustomerName));
		Assert.assertTrue("Existing Cutomer Information is not displayed: ###############", adminCallCenter.existingCustomerName.isDisplayed());
	}
	
	@And("^user waits for the list to populate$")
	public void user_wait_for_List_to_populate() throws InterruptedException{
		Thread.sleep(3000);
	}
	
	@And("^user edits delivery date as today$")
	 public void user_Edits_DeliveryDate_as_Today() throws InterruptedException{
		
		 Thread.sleep(5000);
//		 jse.executeScript("arguments[0].scrollIntoView(true);", adminCallCenter.orderDate);
		 jse.executeScript("window.scrollBy(0,650)");		 
		 wait.until(ExpectedConditions.elementToBeClickable(adminCallCenter.orderDate));
//		 adminCallCenter.asap.click();

		 adminCallCenter.orderDate.click();
	        Thread.sleep(2000);


	        String today = adminCallCenter.getToDay();
	        System.out.println("Today's number: " + today + "\n");
	        List<WebElement> columns = adminCallCenter.calender.findElements(By.tagName("td"));

	        for (WebElement cell : columns) {
	            if (cell.getText().equals(today)) {
	                cell.click();
	                break;
	            }
	        }

	}
	
	@And("^user enters random FirstName$")
	public void user_Enters_random_Name(DataTable dt){
		
		List<List<String>> list = dt.raw();
		Random rand = new Random();
		int randomNumber = rand.nextInt(10000);
		randomNumber += 1;
		String randomStr = Integer.toString(randomNumber);
		String firstName = list.get(0).get(0);
		String randomFirstName = firstName+randomStr;
		try{			 
		wait.until(ExpectedConditions.elementToBeClickable(adminCallCenter.firstName));
		adminCallCenter.firstName.sendKeys(randomFirstName);
		}
		catch (Exception e){
			e.printStackTrace();
			Alert alert = driver.switchTo().alert();
			alert.accept();
			wait.until(ExpectedConditions.visibilityOf(adminCallCenter.firstName));
			adminCallCenter.firstName.sendKeys(list.get(0).get(0));
		}
		
	}
	
	@And("^user selects Credit Card as payment method$")
	public void user_Selects_CC_as_Payement_Method(){
		List<WebElement> list = adminCallCenter.paymentMethod;
		list.get(0).click();
	}
	
	@And("^user enters CC details$")
	public void user_enters_CC_details(DataTable dt) throws InterruptedException{		
		List<List<String>> list = dt.raw();
		Thread.sleep(3000);
//		driver.switchTo().frame(adminCallCenter.frame);
		adminCallCenter.ccNumber.sendKeys(list.get(0).get(1));
		Thread.sleep(500);
		jse.executeScript("window.scrollBy(0,200)");
		Select select = new Select(adminCallCenter.CCexpMnth);
		select.selectByValue("12");
		Thread.sleep(500);
		Select select1 = new Select(adminCallCenter.CCexpYr);
		select1.selectByValue("19");
		adminCallCenter.cvvNumber.sendKeys(list.get(1).get(1));
		
	}
	
	@And("^Click Complete order for CC payment$")
	public void user_Clicks_Complete_Order_For_CCPayment() throws InterruptedException{
		Thread.sleep(4000);
		WebDriverWait waitlocal = new WebDriverWait(driver, 15);

		waitlocal.until(ExpectedConditions.elementToBeClickable(adminCallCenter.ccCompleteOrderBtn));
		//jse.executeScript("arguments[0].click()", adminCallCenter.compOrderBtnFrame);
		adminCallCenter.ccCompleteOrderBtn.click();
//		Thread.sleep(2000);
//		driver.switchTo().parentFrame();
		
	}
	
	@And("^user selects Gift Card as payment method$")
	public void user_selects_Gift_Card_as_payment_method() throws Throwable {
		List<WebElement> list = adminCallCenter.paymentMethod;
		list.get(2).click();
	}
	
	@And("^user Enters Gift card details$")
	public void user_Enters_GiftCard_Details(DataTable dt){
		List<List<String>> list = dt.raw();
		wait.until(ExpectedConditions.elementToBeClickable(adminCallCenter.giftCardNumber));
		adminCallCenter.giftCardNumber.sendKeys(list.get(0).get(1));
		
	}
	
	@And("^Click Complete order for gift card$")
	public void click_Complete_order_for_gift_card() throws Throwable {
		Thread.sleep(4000);
		WebDriverWait waitlocal = new WebDriverWait(driver, 15);

		waitlocal.until(ExpectedConditions.elementToBeClickable(adminCallCenter.giftCardCompOrder));
		adminCallCenter.giftCardCompOrder.click();


	}
	
	@And("^User Selects the existing customer from the customer list$")
	public void user_Selects_Existing_Customer() throws InterruptedException{
		Thread.sleep(2000);
		Select select = new Select(adminCallCenter.customerID);
		select.selectByIndex(0);
		adminCallCenter.selectCustomer.click();
	}
	
	@And("^user select saved Address$")
	public void user_Selects_Saved_Address(){
		wait.until(ExpectedConditions.elementToBeClickable(adminCallCenter.savedAddress));
		Select select = new Select (adminCallCenter.savedAddress);
		select.selectByIndex(1);
	}
	
	@When("^User Clicks Back To Call Center button$")
	public void user_Click_BackToCallCenter() throws InterruptedException{
		adminCallCenter.backToCallCenterBtn.click();
		
		Thread.sleep(5000);
		if(adminCallCenter.isAlertPresent(driver)){
			  Alert alert = driver.switchTo().alert();
			  System.out.println(alert.getText());
			  alert.accept();
			 }
	}
	
	@When("^User Clicks Place another order button$")
	public void user_Click_PlaceAnotherOrder() throws InterruptedException{
		adminCallCenter.startNewOrderBtn.click();
		
		Thread.sleep(5000);
		if(adminCallCenter.isAlertPresent(driver)){
			  Alert alert = driver.switchTo().alert();
			  System.out.println(alert.getText());
			  alert.accept();
			 }
	}
	
	
}
