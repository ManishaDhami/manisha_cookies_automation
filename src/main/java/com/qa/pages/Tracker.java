package com.qa.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.qa.utils.TestBase;

public class Tracker extends TestBase{
	
	
	@FindBy(xpath = "//a[contains(text(),'Tracker')]")
	public WebElement tracker_tab;
	
	@FindBy(xpath = "//*[@id=\"tracker-subnav\"]/form/div/div[2]/button")     //button[@class='btn btn-primary ae-button']
	public WebElement track_order_button;
	
	@FindBy(xpath = "//h4[contains(text(),'No order found!')]")
	public WebElement tracker_message;
	
	@FindBy(xpath = "//span[contains(text(),'Baking')]")
	public WebElement baking;
	
	@FindBy(xpath = "//span[contains(text(),'Out for Delivery')]")
	public WebElement out_for_delivery;
	
	@FindBy(xpath = "//span[contains(text(),'up Next')]")
	public WebElement you_are_up_next;
	
	@FindBy(xpath = "//body[@class='ae-lang-en ae-device-desktop ae-launcher']//div[@tabindex='0']//div//div//div//div//div//div[4]")
	public WebElement store_name;
	
	@FindBy(xpath = "/html[1]/body[1]/div[2]/section[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/div[3]/div[1]/img[1]")
	public WebElement store_pointer;
	
	
	
	WebDriver driver = getDriver();
	WebDriverWait wait = new WebDriverWait(driver, 30);
		
		public Tracker(WebDriver driver) {
			//super(driver);
			PageFactory.initElements(driver, this);

		}
		

}
