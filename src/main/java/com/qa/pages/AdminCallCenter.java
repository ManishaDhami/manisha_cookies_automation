package com.qa.pages;

import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.qa.utils.TestBase;

public class AdminCallCenter extends TestBase{
	
	
	protected WebDriver driver;
	//CREATED PARAMETERIZED CONSTRUCTOR AND PASSING DRIVER INTO IT
	public AdminCallCenter(WebDriver driver) 
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//input[@id='username']")
	public WebElement userName;
	
	@FindBy(xpath="//input[@id='password']")
	public WebElement password;
	
	@FindBy(xpath="//button[contains(text(),'Login')]")
	public WebElement login;
	
	@FindBy(xpath="//i[@class='fa fa-lg fa-fw fa-phone-square']")
	public WebElement callCenter;
	
	@FindBy(xpath="//input[@id='firstname']")
	public WebElement firstName;
	
	@FindBy(xpath="//input[@id='lastname']")
	public WebElement lastName;
	
	@FindBy(xpath="//input[@id='phone']")
	public WebElement phoneNumber;
	
	@FindBy(xpath="//input[@id='email']")
	public WebElement email;
	
	@FindBy(xpath="//div[contains(@class,'widget-body fuelux')]//div[5]//div[1]//div[1]//div[1]//div[1]//label[1]//span[1]")
	public WebElement createAccountYes;
	
	@FindBy(xpath="//input[@id='hidethisfield_autocomplete']")
	public WebElement address;
	
	@FindBy(xpath="//div[@id='step1']//a[contains(@class,'btn btn-success btn-sm btn-continue')][contains(text(),'Continue')]")
	public WebElement continueCustTab;
	
	@FindBy(xpath="//span[contains(text(),'Pickup')]")
	public WebElement pickUpBtn;
	
	@FindBy(xpath="//span[contains(text(),'Delivery')]")
	public WebElement deliveryBtn;
	
	@FindBy(xpath="//a[@id='product-279']")
	public WebElement sixPack;
	
	@FindBy(xpath="//button[contains(@class,'btn btn-success btn-sm assortment')]")
	public WebElement assortment;
	
	@FindBy(xpath="//span[contains(text(),'ADD')]")
	public WebElement addBtn;
	
	@FindBy(xpath="//label[@for='off-campus']")
	public WebElement offCampusBtn;
	
	@FindBy(xpath="//label[@for='on-campus']")
	public WebElement onCampusBtn;
	
	@FindBy(xpath="//select[@id='dorm-selector']")
	public WebElement selCampusBuilding;
	
	@FindBy(xpath="//button[@id='continue-delivery-btn']")
	public WebElement continueDeliveryBtn;
	
	@FindBy(xpath="//a[@id='cart-checkout']")
	public WebElement checkOutBtn;
	
	@FindBy(xpath="//select[@id='devthetime']")
	public WebElement orderTimeDropDown;
	
	@FindBy(xpath="//input[@id='delivery_date_pick']")
	public WebElement orderDate;
	
	@FindBy(xpath="//*[@name='payment_method']")
	public List<WebElement> paymentMethod;
	
	@FindBy(xpath="//div[@id='cod']//span[contains(text(),'Complete Order')]")
	public WebElement completeOrder;
	
	@FindBy(xpath="//button[@id='btnCancelOrder']")
	public WebElement cancelOrder;
	
	@FindBy(xpath="//span[contains(text(),'Edit Order')]")
	public WebElement editOrder;
	
	@FindBy(xpath="//span[contains(text(),'Find Stores')]")
	public WebElement findStore;
	
	@FindBy(xpath="//input[@id='zip']")
	public WebElement zip;
	
	@FindBy(xpath="//span[contains(text(),'ASAP')]")
	public WebElement asap;
	
	@FindBy(xpath="//*[@class='ui-datepicker-calendar']")
	public WebElement calender;
	
	@FindBy(xpath="//label[contains(text(),'Shipping First Name')]")
	public WebElement shipFirstName;
	
	@FindBy(xpath="//h2[contains(text(),'Cart')]")
	public WebElement cart;
	
	@FindBy(xpath="//*[@id='btnNewOrder']")
	public WebElement orderSuccessMsg;
	
	@FindBy(xpath="//a[@id='product-69']")
	public WebElement twoPercentMilk;
	
	@FindBy(xpath="//div[@id='step3']//table[@class='order-table']//tr[1]//td[2]//span[1]")
	public WebElement removeItemFromCart;
	
	@FindBy(xpath="//button[@id='bot2-Msg1']")
	public WebElement deleteProductYes;
	
	@FindBy(xpath="//input[@id='school_cash']")
	public WebElement schoolCash;
	
	@FindBy(xpath="//div[@id='schoolcash']//span[contains(text(),'Complete Order')]")
	public WebElement completeOrderSchoolCash;
	
	@FindBy(xpath="//span[@class='ui-icon ui-icon-circle-triangle-e']")
	public WebElement calenderNextBtn;
	
	@FindBy(xpath="//p[contains(text(),'Cake orders take longer to bake. Please choose a ')]")
	public WebElement coockieCakeValidationMsg;
	
	@FindBy(xpath="//a[@id='product-23']")
	public WebElement cookieCake;
	
	@FindBy(xpath="//span[contains(text(),'Place an Order')]")
	public WebElement placeAnOrderValidationText;
	
	@FindBy(xpath="//a[@class='btn btn-danger btn-sm pull-left clearall']")
	public WebElement custTabClearAllBtn;
	
	@FindBy(xpath="//button[@id='btnClearAll']")
	public WebElement shipTabClearAllBtn;
	
	@FindBy(xpath="//a[@class='dropdown-toggle no-margin userdropdown']//img[@class='online']")
	public WebElement profileImage;
	
	@FindBy(xpath="//strong[contains(text(),'ogout')]")
	public WebElement logOut;
	
	@FindBy(xpath="//button[@id='bot2-Msg1']")
	public WebElement logOutYesBtn;
	
	@FindBy(xpath="//button[@id='bot2-Msg1']")
	public WebElement cancelOrderYesBtn;
	
	@FindBy(xpath="//option[contains(text(),'Prateek Nehra - ')]")
	public WebElement existingCustomerName;
	
	@FindBy(xpath="//input[@id='credit_card']")   //*[@id='ccNumber']
	public WebElement ccNumber;
	
	@FindBy(xpath="//*[@id='expMonth']")
	public WebElement expMonth;
	
	@FindBy(xpath="//select[@id='cc_month']")
	public WebElement CCexpMnth;
	
	@FindBy(xpath="//select[@id='cc_year']")
	public WebElement CCexpYr;
	
	@FindBy(xpath="//input[@id='cvv']")
	public WebElement cvvNumber;
	
	@FindBy(xpath="//*[@id='hpf-iframe']")
	public WebElement frame;
	
	@FindBy(xpath="//*[@id='completeButton']")
	public WebElement compOrderBtnFrame;
	
	@FindBy(xpath="//form[@id='ccpay']//span[contains(text(),'Complete Order')]")
	public WebElement ccCompleteOrderBtn;
	
	@FindBy(xpath="//input[@id='gift_number']")
	public WebElement giftCardNumber;
	
	@FindBy(xpath="//*[@id='giftcard']/table/tbody/tr[3]/td[2]/div[1]/a/span")
	public WebElement giftCardCompOrder;
	
	@FindBy(xpath="//select[@id='customerID']")
	public WebElement customerID;
	
	@FindBy(xpath="//button[contains(text(),'Select Customer')]")
	public WebElement selectCustomer;
	
	@FindBy(xpath="//select[@id='savedaddresses']")
	public WebElement savedAddress;
	
	@FindBy(xpath="/html/body")
	public WebElement closeTabMsg;
	
	@FindBy(xpath="//a[@class='btn btn-danger btn-sm pull-left']")
	public WebElement backToCallCenterBtn;
	
	@FindBy(xpath="//a[contains(text(),'Start New Order')]")
	public WebElement startNewOrderBtn;
	
	@FindBy(xpath="//select[@id='state']")
	public WebElement state;
	
	
	// ################## Method to select today's date #######################
	public String getToDay() {
        // Create a Calendar Object
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());

        // Get Current Day as a number
        int todayInt = calendar.get(Calendar.DAY_OF_MONTH);
        //int tomorrowInt = todayInt+1;
        System.out.println("Today Int: " + todayInt + "\n");

        // Integer to String Conversion
        String todayStr = Integer.toString(todayInt);
        System.out.println("Today Str: " + todayStr + "\n");

        return todayStr;
	}
	
	// ################## Method to select tomorrow's date #######################
	public String getCurrentDay() {
        // Create a Calendar Object
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());

        // Get Current Day as a number
        int todayInt = calendar.get(Calendar.DAY_OF_MONTH);
        int tomorrowInt = todayInt+1;
        System.out.println("Tomorrow Int: " + todayInt + "\n");

        // Integer to String Conversion
        String tomorrowStr = Integer.toString(tomorrowInt);
        System.out.println("Tomorrow Str: " + tomorrowStr + "\n");

        return tomorrowStr;
	}
	
	public boolean isAlertPresent(WebDriver driver){
		 try{
		  driver.switchTo().alert();
		  return true;
		 }catch(NoAlertPresentException ex){
		  return false;
		 }
		}
	
	public void logout_AdminSite(WebDriver driver){
		WebDriverWait wait = new WebDriverWait(driver, 20);
		profileImage.click();
		wait.until(ExpectedConditions.elementToBeClickable(logOut));
		logOut.click();
		wait.until(ExpectedConditions.elementToBeClickable(logOutYesBtn));
		logOutYesBtn.click();
	}
	

}
