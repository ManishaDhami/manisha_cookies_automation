package com.qa.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.utils.TestBase;

public class AdminStores extends TestBase{

	
	protected WebDriver driver;
	//CREATED PARAMETERIZED CONSTRUCTOR AND PASSING DRIVER INTO IT
	public AdminStores(WebDriver driver) 
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//input[@id='username']")
	public WebElement userName;
	
	@FindBy(xpath="//input[@id='password']")
	public WebElement password;
	
	@FindBy(xpath="//button[contains(text(),'Login')]")
	public WebElement login;
	
	@FindBy(xpath="//span[contains(text(),'Stores')]")
	public WebElement stores;
	
	@FindBy(xpath="//input[@class='form-control']")
	public WebElement storeSearchTextBox;
	
	@FindBy(xpath="//tr[1]//td[9]//a[1]")
	public WebElement paymentsLink;
	
	@FindBy(xpath="//select[@id='selected-payment-processor']")
	public WebElement ccPaymentProcessor; 
	
	@FindBy(xpath="//a[@class='dropdown-toggle no-margin userdropdown']//img[@class='online']")
	public WebElement profileIcon;
		
	
	@FindBy(xpath="//strong[contains(text(),'ogout')]")
	public WebElement logout;
	
	@FindBy(xpath="//button[@id='bot2-Msg1']")
	public WebElement confirmLogout;
	
	@FindBy(xpath="//button[@id='update-payment-processor-btn']")
	public WebElement updateBtn;
	
	@FindBy(xpath="//p[contains(text(),'Payment processor updated')]")
	public WebElement popupUpdateSuccessMsg;
	
	@FindBy(xpath="//tr[1]//td[2]")
	public WebElement cardStatus;
	
	@FindBy(xpath="//th[contains(text(),'Status')]")
	public WebElement headerStatus;
	
//	public void login (String userName1, String password1){
//		userName.sendKeys(userName1);
//		password.sendKeys(password1);	
//		login.click();
//	}
	public void Admin_login(){
		try{

			if(userName.isDisplayed()){
				userName.sendKeys(prop.getProperty("adminSiteUserName"));
				password.sendKeys(prop.getProperty("adminSitePassword"));
				login.click();
			}}
			catch (Exception e){
			if(profileIcon.isDisplayed()){
				System.out.println("User already logged in");
			}}
	}
	
}
